package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductBusinessService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import com.springcloud.intelligentagriculture.model.WorkTask;
import com.springcloud.intelligentagriculture.repository.ProductBusinessRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductBusiness API", produces = "application/json", tags = {"经营主体-经营产品"})

public class ProductBusinessController {

    @Autowired
    ProductBusinessRepository productBusinessRepository;
    @Autowired
    ProductBusinessService productBusinessService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/product_business/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductBusiness(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                  @RequestParam(defaultValue = "20") Integer pageSize,
                                                                  @RequestParam(defaultValue = "id") String sortBy,
                                                                  @RequestParam(defaultValue = "0") Integer company_id
    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductBusiness> list = productBusinessService.getAllProductBusiness(pageNo, pageSize, underscoreSortBy.toString(), company_id);

        List<ProductBusiness> totallist = productBusinessRepository.findAllBySearch(company_id);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @DeleteMapping("/product_business/{id}")
    public ResponseEntity<?> deleteProductBusiness(@PathVariable(value = "id") Long id) {
        ProductBusiness productBusiness = productBusinessRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductBusiness", "id", id));

        productBusinessRepository.delete(productBusiness);

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value="/product_business/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ProductBusiness createProductBusiness(MultipartFile file, ProductBusiness productBusiness ) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productBusiness.setProductProfile(fileUploadUri);
        }

        if (!productBusinessRepository.existsById(productBusiness.getId()))
            return productBusinessRepository.save(productBusiness);
        else
            return null;
    }
}