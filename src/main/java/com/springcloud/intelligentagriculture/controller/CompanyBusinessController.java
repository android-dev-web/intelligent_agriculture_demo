package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.CompanyBusinessService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.CompanyBusiness;
import com.springcloud.intelligentagriculture.repository.CompanyBusinessRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "CompanyBusiness API", produces = "application/json", tags = {"经营主体"})

public class CompanyBusinessController {

    @Autowired
    CompanyBusinessRepository companyBusinessRepository;
    @Autowired
    CompanyBusinessService companyBusinessService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/company_business/all")
    @ResponseBody
    public ResponseEntity<String> getAllcompanyBusiness(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                       @RequestParam(defaultValue = "20") Integer pageSize,
                                                                       @RequestParam(defaultValue = "id") String sortBy,
                                                                       @RequestParam(defaultValue = "0") String product_type,
                                                                       @RequestParam(defaultValue = "0") String company_scale
    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<CompanyBusiness> list = companyBusinessService.getAllcompanyBusiness(pageNo, pageSize, underscoreSortBy.toString(), product_type, company_scale);

        List<CompanyBusiness> totallist = companyBusinessRepository.findAllBy(product_type, company_scale);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/company_business/get/{id}")
    public CompanyBusiness getcompanyBusiness(@PathVariable(value = "id") Long id) {

        CompanyBusiness companyBusiness = companyBusinessRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyBusiness", "id", id));

        return companyBusiness;
    }

    @RequestMapping(value="/company_business/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public CompanyBusiness updateCompanyBusiness(@PathVariable(value = "id") Long id,
                                                 CompanyBusiness updatedCompanyBusiness, MultipartFile file) {

        CompanyBusiness companyBusiness = companyBusinessRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyBusiness", "id", id));

        if(file != null)
        {
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            companyBusiness.setProfileImage(fileUploadUri);
        }

        companyBusiness.setCompanyAddress(updatedCompanyBusiness.getCompanyAddress());
        companyBusiness.setCompanyName(updatedCompanyBusiness.getCompanyName());
        companyBusiness.setCompanyProfile(updatedCompanyBusiness.getCompanyProfile());
        companyBusiness.setCompanyScale(updatedCompanyBusiness.getCompanyScale());
        companyBusiness.setContactWay(updatedCompanyBusiness.getContactWay());
        companyBusiness.setCreditCode(updatedCompanyBusiness.getCreditCode());
        companyBusiness.setLegalPerson(updatedCompanyBusiness.getLegalPerson());
        companyBusiness.setProductType(updatedCompanyBusiness.getProductType());
        companyBusiness.setSupervisionNature(updatedCompanyBusiness.getSupervisionNature());
        companyBusiness.setUpdateTime(updatedCompanyBusiness.getUpdateTime());
        companyBusiness.setUpdateUserId(updatedCompanyBusiness.getUpdateUserId());

        CompanyBusiness updateResult = companyBusinessRepository.save(companyBusiness);
        return updateResult;
    }



    @DeleteMapping("/company_business/delete/{id}")
    public ResponseEntity<?> deleteCompanyBusiness(@PathVariable(value = "id") Long id) {
        CompanyBusiness companyBusiness = companyBusinessRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyBusiness", "id", id));

        companyBusinessRepository.delete(companyBusiness);

        return ResponseEntity.ok().build();
    }


}