package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.InputsUseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.InputsUse;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.InputsUseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")

@Api(description = "InputsUse API", produces = "application/json", tags = {"投入品管理/使用信息"})

public class InputsUseController {

    @Autowired
    InputsUseRepository inputsUseRepository;
    @Autowired
    InputsUseService inputsUseService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/inputsUse/all")
    @ResponseBody
    public ResponseEntity<String> getAllInputsUses(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy
                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<InputsUse> list = inputsUseService.getAllInputsUses(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",inputsUseRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value = "/inputsUse/create", consumes = {"multipart/form-data"})
    public InputsUse createInputsUse(MultipartFile file, InputsUse inputsUse) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            inputsUse.setInputProfiles(fileUploadUri);
        }

        if (!inputsUseRepository.existsById(inputsUse.getId()))
            return inputsUseRepository.save(inputsUse);
        else
            return null;
    }


    @GetMapping("/inputsUse/get/{id}")
    public InputsUse getInputsUseId(@PathVariable(value = "id") Long id) {
        InputsUse inputsUse = inputsUseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsUse", "id", id));
        return inputsUse;
    }

    @RequestMapping(value="/inputsUse/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public InputsUse updateInputsUse(MultipartFile file, @PathVariable(value = "id") Long id,
                                     InputsUse updatedInputsUse) {

        InputsUse inputsUse = inputsUseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsUse", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            inputsUse.setInputProfiles(fileUploadUri);
        }

        inputsUse.setCompanyId(updatedInputsUse.getCompanyId());
        inputsUse.setSampleName(updatedInputsUse.getSampleName());
        inputsUse.setAmount(updatedInputsUse.getAmount());
        inputsUse.setInputProfiles(updatedInputsUse.getInputProfiles());
        inputsUse.setUpdateTime(updatedInputsUse.getUpdateTime());
        inputsUse.setUpdateUserId(updatedInputsUse.getUpdateUserId());

        InputsUse updateResult = inputsUseRepository.save(inputsUse);
        return updateResult;
    }


    @DeleteMapping("/inputsUse/delete/{id}")
    public ResponseEntity<?> deleteInputsUse(@PathVariable(value = "id") Long id) {
        InputsUse inputsUse = inputsUseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsUse", "id", id));

        inputsUseRepository.delete(inputsUse);

        return ResponseEntity.ok().build();
    }
}