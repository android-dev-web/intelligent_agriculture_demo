package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.JobDefinitionService;
import com.springcloud.intelligentagriculture.Service.ProductionRecordService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.JobDefinitionRepository;
import com.springcloud.intelligentagriculture.repository.ProductionRecordRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductionRecord API", produces = "application/json", tags = {"生产记录"})

public class ProductionRecordController {

    @Autowired
    ProductionRecordRepository productionRecordRepository;
    @Autowired
    ProductionRecordService productionRecordService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/production_record/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductionRecord(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                   @RequestParam(defaultValue = "20") Integer pageSize,
                                                                   @RequestParam(defaultValue = "id") String sortBy

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductionRecord> list = productionRecordService.getAllProductionRecord(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",productionRecordRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/production_record/get/{id}")
    public ProductionRecord getProductionRecord(@PathVariable(value = "id") Long id) {

        ProductionRecord productionRecord = productionRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductionRecord", "id", id));;

        return productionRecord;
    }

    @RequestMapping(value="/production_record/create", method = RequestMethod.POST,consumes = {"multipart/form-data"})
    public ProductionRecord createProductionRecord(MultipartFile file,  ProductionRecord productionRecord ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productionRecord.setProductionProfiles(fileUploadUri);
        }

        if (!productionRecordRepository.existsById(productionRecord.getId()))
            return productionRecordRepository.save(productionRecord);
        else
            return null;
    }

    @RequestMapping(value="/production_record/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public ProductionRecord updateProductionRecord(MultipartFile file, @PathVariable(value = "id") Long id, ProductionRecord updatedProductionRecord )
    {

        ProductionRecord productionRecord = productionRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductionRecord", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productionRecord.setProductionProfiles(fileUploadUri);
        }

        productionRecord.setProductId(updatedProductionRecord.getProductId());
        productionRecord.setProductName(updatedProductionRecord.getProductName());
        productionRecord.setProductionQuantity(updatedProductionRecord.getProductionQuantity());
        productionRecord.setProductionArea(updatedProductionRecord.getProductionArea());
        productionRecord.setProductionTime(updatedProductionRecord.getProductionTime());

        productionRecord.setUpdateTime(updatedProductionRecord.getUpdateTime());
        productionRecord.setUpdateUserId(updatedProductionRecord.getUpdateUserId());

        ProductionRecord updatedResult = productionRecordRepository.save(productionRecord);
        return updatedResult;
    }

    @DeleteMapping("/production_record/delete/{id}")
    public ResponseEntity<?> deleteProductionRecord(@PathVariable(value = "id") Long id) {
        ProductionRecord productionRecord = productionRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductionRecord", "id", id));

        productionRecordRepository.delete(productionRecord);

        return ResponseEntity.ok().build();
    }

}