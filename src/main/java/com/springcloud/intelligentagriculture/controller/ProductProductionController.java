package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductProductionService;
import com.springcloud.intelligentagriculture.model.ProductProduction;
import com.springcloud.intelligentagriculture.repository.ProductProductionRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductProduction API", produces = "application/json", tags = {"产品"})
public class ProductProductionController {

    @Autowired
    ProductProductionService productProductionService;
    @Autowired
    ProductProductionRepository productProductionRepository;

    @GetMapping("/product_production/all")
    @ResponseBody
    public ResponseEntity<String> getAllGrades(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                @RequestParam(defaultValue = "20") Integer pageSize,
                                                                @RequestParam(defaultValue = "productId") String sortBy,
                                                                @RequestParam(defaultValue = "0") Integer company_id) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);
        List<ProductProduction> list = productProductionService.getAllProductions(pageNo, pageSize, underscoreSortBy.toString(), company_id);

        List<ProductProduction> totallist = productProductionRepository.findAllBySearch(company_id);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }



    @GetMapping("/product_production/name")
    @ResponseBody
    public ResponseEntity<List<ProductProduction>> getNames( @RequestParam(defaultValue = "0") Integer productid )  {

        List<ProductProduction> list = productProductionService.getNameProductProductions(productid);
        return new ResponseEntity<List<ProductProduction>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/product_production/create")
    public ProductProduction registerGrade(@Valid @RequestBody ProductProduction productProduction) {
        if (!productProductionRepository.existsById(productProduction.getProductId()))
            return productProductionRepository.save(productProduction);
        else
            return null;
    }
}
