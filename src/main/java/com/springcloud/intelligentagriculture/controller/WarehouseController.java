package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.QualityStandard;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "Warehouse API", produces = "application/json", tags = {"生产主体-仓储环境"})

public class WarehouseController {

    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    WarehouseService warehouseService;


    @GetMapping("/warehose/all")
    @ResponseBody
    public ResponseEntity<String> getAllWarehouses(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(defaultValue = "0") Integer company_id
    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<Warehouse> list = warehouseService.getAllWarehouses(pageNo, pageSize, underscoreSortBy.toString(), company_id);

        List<Warehouse> totallist = warehouseRepository.findAllBySearch(company_id);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/warehose/create")
    public Warehouse registerWarehouse(@Valid @RequestBody Warehouse warehouse) {
        if (!warehouseRepository.existsById(warehouse.getId()))
            return warehouseRepository.save(warehouse);
        else
            return null;
    }


    @GetMapping("/warehose/get/{id}")
    public Warehouse getWarehouseId(@PathVariable(value = "id") Long id) {
        Warehouse warehouse = warehouseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Warehouse", "id", id));
        return warehouse;
    }

    @RequestMapping(value="/warehose/update/{id}", method = RequestMethod.PUT)
    public Warehouse updateWarehouse(@PathVariable(value = "id") Long id,
                                                 @Valid @RequestBody Warehouse updatedWarehouse) {

        Warehouse warehouse = warehouseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Warehouse", "id", id));


        warehouse.setCompanyId(updatedWarehouse.getCompanyId());

        warehouse.setUpdateTime(updatedWarehouse.getUpdateTime());
        warehouse.setUpdateUserId(updatedWarehouse.getUpdateUserId());
        warehouse.setWarehouseAddress(updatedWarehouse.getWarehouseAddress());
        warehouse.setWarehouseArea(updatedWarehouse.getWarehouseArea());
        warehouse.setWarehouseName(updatedWarehouse.getWarehouseName());
        warehouse.setWarehouseScope(updatedWarehouse.getWarehouseScope());

        Warehouse updateResult = warehouseRepository.save(warehouse);
        return updateResult;
    }


    @DeleteMapping("/warehouse/delete/{id}")
    public ResponseEntity<?> deleteWarehouse(@PathVariable(value = "id") Long id) {
        Warehouse warehouse = warehouseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Warehouse", "id", id));

        warehouseRepository.delete(warehouse);

        return ResponseEntity.ok().build();
    }
}