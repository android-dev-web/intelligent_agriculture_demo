package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.ShareFilesService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.ShareFiles;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.ShareFilesRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")

@Api(description = "ShareFiles API", produces = "application/json", tags = {"共享文件"})

public class ShareFilesController {

    @Autowired
    ShareFilesRepository shareFilesRepository;
    @Autowired
    ShareFilesService shareFilesService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/shareFiles/all")
    @ResponseBody
    public ResponseEntity<String> getAllShareFiless(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(defaultValue = "-1") Integer isVisible                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ShareFiles> list = shareFilesService.getAllShareFiless(pageNo, pageSize, underscoreSortBy.toString(), isVisible);

        List<ShareFiles> totalList = shareFilesRepository.findAllBySearch(isVisible);

        JSONObject entity = new JSONObject();
        entity.put("total",totalList.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value="/shareFiles/create")
    public ShareFiles createShareFiles(@Valid @RequestBody ShareFiles shareFiles) {
        if (!shareFilesRepository.existsById(shareFiles.getId()))
            return shareFilesRepository.save(shareFiles);
        else
            return null;
    }


    @GetMapping("/shareFiles/get/{id}")
    public ShareFiles getShareFilesId(@PathVariable(value = "id") Long id) {
        ShareFiles shareFiles = shareFilesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFiles", "id", id));
        return shareFiles;
    }

    @RequestMapping(value="/shareFiles/update/{id}", method = RequestMethod.PUT)
    public ShareFiles updateShareFiles(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody ShareFiles updatedShareFiles) {

        ShareFiles shareFiles = shareFilesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFiles", "id", id));

        shareFiles.setFileName(updatedShareFiles.getFileName());
        shareFiles.setCreater(updatedShareFiles.getCreater());
        shareFiles.setIsVisible(updatedShareFiles.getIsVisible());
        shareFiles.setUpdateTime(updatedShareFiles.getUpdateTime());
        shareFiles.setUpdateUserId(updatedShareFiles.getUpdateUserId());

        ShareFiles updateResult = shareFilesRepository.save(shareFiles);
        return updateResult;
    }


    @DeleteMapping("/shareFiles/delete/{id}")
    public ResponseEntity<?> deleteShareFiles(@PathVariable(value = "id") Long id) {
        ShareFiles shareFiles = shareFilesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFiles", "id", id));

        shareFilesRepository.delete(shareFiles);

        return ResponseEntity.ok().build();
    }

}