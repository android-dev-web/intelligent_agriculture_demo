package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.JobDefinitionService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.Service.WorkTaskService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.JobDefinitionRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "WorkTask API", produces = "application/json", tags = {"工作任务"})

public class WorkTaskController {

    @Autowired
    WorkTaskRepository workTaskRepository;
    @Autowired
    WorkTaskService workTaskService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/work_task/all")
    @ResponseBody
    public ResponseEntity<String> getAllWorkTask(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                                                    @RequestParam(defaultValue = "id") String sortBy,
                                                                    @RequestParam(defaultValue = "") String fromDate,
                                                                    @RequestParam(defaultValue = "") String toDate

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<WorkTask> list = workTaskService.getAllWorkTask(pageNo, pageSize, underscoreSortBy.toString(), fromDate, toDate);

        List<WorkTask> totallist = workTaskRepository.findAllBySearch(fromDate, toDate);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/work_task/get/{id}")
    public WorkTask getWorkTask(@PathVariable(value = "id") Long id) {

        WorkTask workTask = workTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("WorkTask", "id", id));;

        return workTask;
    }

    @RequestMapping(value="/work_task/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public WorkTask createWorkTask(MultipartFile file,  WorkTask workTask ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file, "work_task");
            String fileUploadUri = "/uploads/work_task/" + fileName.toString();
            workTask.setWorkTaskProfiles(fileUploadUri);
        }

        if (!workTaskRepository.existsById(workTask.getId()))
            return workTaskRepository.save(workTask);
        else
            return null;
    }

    @RequestMapping(value="/work_task/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public WorkTask updateWorkTask(MultipartFile file, @PathVariable(value = "id") Long id, WorkTask updatedWorkTask )
    {

        WorkTask workTask = workTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("WorkTask", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file, "work_task");
            String fileUploadUri = "/uploads/work_task/" + fileName.toString();
            workTask.setWorkTaskProfiles(fileUploadUri);
        }

        workTask.setReleaseTime(updatedWorkTask.getReleaseTime());
        workTask.setReleasePerson(updatedWorkTask.getReleasePerson());
        workTask.setTitle(updatedWorkTask.getTitle());
        workTask.setContent(updatedWorkTask.getContent());

        workTask.setUpdateTime(updatedWorkTask.getUpdateTime());
        workTask.setUpdateUserId(updatedWorkTask.getUpdateUserId());

        WorkTask updatedResult = workTaskRepository.save(workTask);
        return updatedResult;
    }


    @DeleteMapping("/work_task/delete/{id}")
    public ResponseEntity<?> deleteWorkTask(@PathVariable(value = "id") Long id) {
        WorkTask workTask = workTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("WorkTask", "id", id));

        workTaskRepository.delete(workTask);

        return ResponseEntity.ok().build();
    }
}