package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.JobDefinitionService;
import com.springcloud.intelligentagriculture.Service.NoticeService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.Service.WorkTaskService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.JobDefinitionRepository;
import com.springcloud.intelligentagriculture.repository.NoticeRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "Notice API", produces = "application/json", tags = {"通知管理"})

public class NoticeController {

    @Autowired
    NoticeRepository noticeRepository;
    @Autowired
    NoticeService noticeService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/notice/all")
    @ResponseBody
    public ResponseEntity<String> getAllNotice(@RequestParam(defaultValue = "0") Integer pageNo,
                                                         @RequestParam(defaultValue = "20") Integer pageSize,
                                                         @RequestParam(defaultValue = "id") String sortBy,
                                                         @RequestParam(defaultValue = "0") String emergencyDegree

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<Notice> list = noticeService.getAllNotice(pageNo, pageSize, underscoreSortBy.toString(), emergencyDegree);

        List<Notice> totallist = noticeRepository.findAllBySearch(emergencyDegree);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/notice/get/{id}")
    public Notice getNotice(@PathVariable(value = "id") Long id) {

        Notice notice = noticeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Notice", "id", id));;

        return notice;
    }

    @RequestMapping(value="/notice/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Notice createNotice(MultipartFile file,  Notice notice ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            notice.setNoticeProfiles(fileUploadUri);
        }

        if (!noticeRepository.existsById(notice.getId()))
            return noticeRepository.save(notice);
        else
            return null;
    }

    @RequestMapping(value="/notice/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public Notice updateNotice(MultipartFile file, @PathVariable(value = "id") Long id, Notice updatedNotice )
    {

        Notice notice = noticeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Notice", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            notice.setNoticeProfiles(fileUploadUri);
        }

        notice.setReleaseTime(updatedNotice.getReleaseTime());
        notice.setReleasePerson(updatedNotice.getReleasePerson());
        notice.setTitle(updatedNotice.getTitle());
        notice.setContent(updatedNotice.getContent());
        notice.setEmergencyDegree(updatedNotice.getEmergencyDegree());
        notice.setType(updatedNotice.getType());
        notice.setTimingRelease(updatedNotice.getTimingRelease());

        notice.setUpdateTime(updatedNotice.getUpdateTime());
        notice.setUpdateUserId(updatedNotice.getUpdateUserId());

        Notice updatedResult = noticeRepository.save(notice);
        return updatedResult;
    }


    @DeleteMapping("/notice/delete/{id}")
    public ResponseEntity<?> deleteNotice(@PathVariable(value = "id") Long id) {
        Notice notice = noticeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Notice", "id", id));

        noticeRepository.delete(notice);

        return ResponseEntity.ok().build();
    }
}