package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.InternalMessageService;
import com.springcloud.intelligentagriculture.Service.ProductBatchService;
import com.springcloud.intelligentagriculture.Service.ProductionStandardService;
import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.InternalMessageRepository;
import com.springcloud.intelligentagriculture.repository.ProductBatchRepository;
import com.springcloud.intelligentagriculture.repository.ProductionStandardRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductBatch API", produces = "application/json", tags = {"生产主体-主营产品-产品批次"})

public class ProductBatchController {

    @Autowired
    ProductBatchRepository productBatchRepository;
    @Autowired
    ProductBatchService productBatchService;


    @GetMapping("/product_batch/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductBatch(@RequestParam(defaultValue = "0") Integer pageNo,
                                                        @RequestParam(defaultValue = "20") Integer pageSize,
                                                        @RequestParam(defaultValue = "id") String sortBy,
                                                        @RequestParam(defaultValue = "0") Long productId


    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductBatch> list = productBatchService.getAllProductBatch(pageNo, pageSize, underscoreSortBy.toString(), productId);

        List<ProductBatch> totallist = productBatchRepository.findAllBySearch(productId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping("/product_batch/create")
    public ProductBatch createProductBatch(@Valid @RequestBody ProductBatch productBatch) {
        if (!productBatchRepository.existsById(productBatch.getId()))
            return productBatchRepository.save(productBatch);
        else
            return null;
    }

}