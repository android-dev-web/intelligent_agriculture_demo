package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.AquaticStatisticsService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.AquaticStatistics;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.AquaticStatisticsRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "AquaticStatistics API", produces = "application/json", tags = {"水产统计"})

public class AquaticStatisticsController {

    @Autowired
    AquaticStatisticsRepository aquaticStatisticsRepository;
    @Autowired
    AquaticStatisticsService aquaticStatisticsService;

    private static final Logger logger = LoggerFactory.getLogger(AquaticStatisticsController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/aquatic_statistics/all")
    @ResponseBody
    public ResponseEntity<String> getAllAquaticStatisticss(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy
                                                            ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<AquaticStatistics> list = aquaticStatisticsService.getAllAquaticStatistics(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",aquaticStatisticsRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value="/aquatic_statistics/create")
    public ResponseEntity<String> createAquaticStatistics(@Valid @RequestBody List<AquaticStatistics> aquaticStatistics) {
        List<AquaticStatistics> listAqua = new List<AquaticStatistics>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<AquaticStatistics> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(AquaticStatistics aquaticStatistics) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends AquaticStatistics> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends AquaticStatistics> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public AquaticStatistics get(int index) {
                return null;
            }

            @Override
            public AquaticStatistics set(int index, AquaticStatistics element) {
                return null;
            }

            @Override
            public void add(int index, AquaticStatistics element) {

            }

            @Override
            public AquaticStatistics remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<AquaticStatistics> listIterator() {
                return null;
            }

            @Override
            public ListIterator<AquaticStatistics> listIterator(int index) {
                return null;
            }

            @Override
            public List<AquaticStatistics> subList(int fromIndex, int toIndex) {
                return null;
            }
        };

        for(int i = 0; i < aquaticStatistics.size() ; i++) {
            if (!aquaticStatisticsRepository.existsById(aquaticStatistics.get(i).getId()))
            {
                aquaticStatisticsRepository.save(aquaticStatistics.get(i));
//                listAqua.add(aquaticStatisticsRepository.save(aquaticStatistics.get(i)));
//                logger.info(aquaticStatistics.get(i).getCity());
//                logger.info(aquaticStatistics.get(i).getCreateTime().toString());
//                logger.info(aquaticStatistics.get(i).getCreateUserId().toString());
//                logger.info(aquaticStatistics.get(i).getDetectionRoomName());
//                logger.info(aquaticStatistics.get(i).getDetectionRoomNum());
//                logger.info(aquaticStatistics.get(i).getDetectionTime().toString());
//                logger.info(aquaticStatistics.get(i).getDistrict());
//                logger.info(aquaticStatistics.get(i).getFuraMeta());
//                logger.info(aquaticStatistics.get(i).getId().toString());
//                logger.info(aquaticStatistics.get(i).getInspectedUnit());
//                logger.info(aquaticStatistics.get(i).getMalaGreen());
//                logger.info(aquaticStatistics.get(i).getSampleName());
//                logger.info(aquaticStatistics.get(i).getSampleScientificName());
//                logger.info(aquaticStatistics.get(i).getSampleName());
//                logger.info(aquaticStatistics.get(i).getUpdateTime().toString());
//                logger.info(aquaticStatistics.get(i).getUpdateUserId().toString());
            }
        }
//
//        return listAqua;

//        if (!aquaticStatisticsRepository.existsById(aquaticStatistics.getId()))
//            return aquaticStatisticsRepository.save(aquaticStatistics);
//        else
//            return null;

        return new ResponseEntity<String>("Success", new HttpHeaders(), HttpStatus.OK);
    }


    @GetMapping("/aquatic_statistics/get/{id}")
    public AquaticStatistics getAquaticStatisticsId(@PathVariable(value = "id") Long id) {
        AquaticStatistics aquaticStatistics = aquaticStatisticsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AquaticStatistics", "id", id));
        return aquaticStatistics;
    }

    @RequestMapping(value="/aquatic_statistics/update/{id}", method = RequestMethod.PUT)
    public AquaticStatistics updateAquaticStatistics(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody AquaticStatistics updatedAquaticStatistics) {

        AquaticStatistics aquaticStatistics = aquaticStatisticsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AquaticStatistics", "id", id));

        aquaticStatistics.setSerialNum(updatedAquaticStatistics.getSerialNum());
        aquaticStatistics.setSampleName(updatedAquaticStatistics.getSampleName());
        aquaticStatistics.setSampleScientificName(updatedAquaticStatistics.getSampleScientificName());
        aquaticStatistics.setSampleSerialNum(updatedAquaticStatistics.getSampleSerialNum());
        aquaticStatistics.setCity(updatedAquaticStatistics.getCity());
        aquaticStatistics.setDistrict(updatedAquaticStatistics.getDistrict());
        aquaticStatistics.setInspectedUnit(updatedAquaticStatistics.getInspectedUnit());
        aquaticStatistics.setSampleType(updatedAquaticStatistics.getSampleType());
        aquaticStatistics.setFuraMeta(updatedAquaticStatistics.getFuraMeta());
        aquaticStatistics.setMalaGreen(updatedAquaticStatistics.getMalaGreen());
        aquaticStatistics.setDetectionRoomNum(updatedAquaticStatistics.getDetectionRoomNum());
        aquaticStatistics.setDetectionRoomName(updatedAquaticStatistics.getDetectionRoomName());
        aquaticStatistics.setDetectionTime(updatedAquaticStatistics.getDetectionTime());
        aquaticStatistics.setUpdateTime(updatedAquaticStatistics.getUpdateTime());
        aquaticStatistics.setUpdateUserId(updatedAquaticStatistics.getUpdateUserId());

        AquaticStatistics updateResult = aquaticStatisticsRepository.save(aquaticStatistics);
        return updateResult;
    }


    @DeleteMapping("/aquatic_statistics/delete/{id}")
    public ResponseEntity<?> deleteAquaticStatistics(@PathVariable(value = "id") Long id) {
        AquaticStatistics aquaticStatistics = aquaticStatisticsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AquaticStatistics", "id", id));

        aquaticStatisticsRepository.delete(aquaticStatistics);

        return ResponseEntity.ok().build();
    }

}