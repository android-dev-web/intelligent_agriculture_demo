package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.SampleCheck;
import com.springcloud.intelligentagriculture.model.SampleCheckResult;
import com.springcloud.intelligentagriculture.model.SupervisionRecord;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "SampleCheck API", produces = "application/json", tags = {"例行抽样/抽样计划"})

public class SampleCheckController {

    @Autowired
    SampleCheckRepository sampleCheckRepository;
    @Autowired
    SampleCheckService sampleCheckService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/sample_check/all")
    @ResponseBody
    public ResponseEntity<String> getAllSampleCheck(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<SampleCheck> list = sampleCheckService.getAllSampleCheck(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",sampleCheckRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/sample_check/get/{id}")
    public SampleCheck getSampleCheck(@PathVariable(value = "id") Long id) {

        SampleCheck sampleCheck = sampleCheckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("SampleCheck", "id", id));;

        return sampleCheck;
    }

    @RequestMapping(value="/sample_check/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public SampleCheck createSampleCheck(MultipartFile file,  SampleCheck sampleCheck ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            sampleCheck.setCheckFiles(fileUploadUri);
        }

        if (!sampleCheckRepository.existsById(sampleCheck.getId()))
            return sampleCheckRepository.save(sampleCheck);
        else
            return null;
    }

    @RequestMapping(value="/sample_check/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public SampleCheck updateSampleCheck(MultipartFile file, @PathVariable(value = "id") Long id, SampleCheck sampleCheckDetails )
    {

        SampleCheck sampleCheck = sampleCheckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("SampleCheck", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            sampleCheck.setCheckFiles(fileUploadUri);
        }

        sampleCheck.setSampleId(sampleCheckDetails.getSampleId());
        sampleCheck.setSampleName(sampleCheckDetails.getSampleName());
        sampleCheck.setSampleTime(sampleCheckDetails.getSampleTime());
        sampleCheck.setCheckPerson(sampleCheckDetails.getCheckPerson());

        sampleCheck.setUpdateTime(sampleCheckDetails.getUpdateTime());
        sampleCheck.setUpdateUserId(sampleCheckDetails.getUpdateUserId());

        SampleCheck updatedistributor = sampleCheckRepository.save(sampleCheck);
        return updatedistributor;
    }



}