package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.InputsPurchaseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.InputsPurchase;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.InputsPurchaseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")

@Api(description = "InputsPurchase API", produces = "application/json", tags = {"投入品管理/采购信息"})

public class InputsPurchaseController {

    @Autowired
    InputsPurchaseRepository inputsPurchaseRepository;
    @Autowired
    InputsPurchaseService inputsPurchaseService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/inputsPurchase/all")
    @ResponseBody
    public ResponseEntity<String> getAllInputsPurchases(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy
                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<InputsPurchase> list = inputsPurchaseService.getAllInputsPurchases(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",inputsPurchaseRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value = "/inputsPurchase/create", consumes = {"multipart/form-data"})
    public InputsPurchase createInputsPurchase(MultipartFile file, InputsPurchase inputsPurchase) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            inputsPurchase.setInputProfiles(fileUploadUri);
        }

        if (!inputsPurchaseRepository.existsById(inputsPurchase.getId()))
            return inputsPurchaseRepository.save(inputsPurchase);
        else
            return null;
    }


    @GetMapping("/inputsPurchase/get/{id}")
    public InputsPurchase getInputsPurchaseId(@PathVariable(value = "id") Long id) {
        InputsPurchase inputsPurchase = inputsPurchaseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsPurchase", "id", id));
        return inputsPurchase;
    }

    @RequestMapping(value="/inputsPurchase/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public InputsPurchase updateInputsPurchase(MultipartFile file, @PathVariable(value = "id") Long id,
                                     InputsPurchase updatedInputsPurchase) {

        InputsPurchase inputsPurchase = inputsPurchaseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsPurchase", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            inputsPurchase.setInputProfiles(fileUploadUri);
        }

        inputsPurchase.setCompanyId(updatedInputsPurchase.getCompanyId());
        inputsPurchase.setSampleName(updatedInputsPurchase.getSampleName());
        inputsPurchase.setAmount(updatedInputsPurchase.getAmount());
        inputsPurchase.setInputProfiles(updatedInputsPurchase.getInputProfiles());
        inputsPurchase.setCompanyId(updatedInputsPurchase.getCompanyId());
        inputsPurchase.setUpdateTime(updatedInputsPurchase.getUpdateTime());
        inputsPurchase.setUpdateUserId(updatedInputsPurchase.getUpdateUserId());

        InputsPurchase updateResult = inputsPurchaseRepository.save(inputsPurchase);
        return updateResult;
    }


    @DeleteMapping("/inputsPurchase/delete/{id}")
    public ResponseEntity<?> deleteInputsPurchase(@PathVariable(value = "id") Long id) {
        InputsPurchase inputsPurchase = inputsPurchaseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("InputsPurchase", "id", id));

        inputsPurchaseRepository.delete(inputsPurchase);

        return ResponseEntity.ok().build();
    }
}