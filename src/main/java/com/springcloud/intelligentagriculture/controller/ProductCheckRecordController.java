package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.ProductCheckRecordService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductCheckRecord;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.ProductCheckRecordRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductCheckRecord API", produces = "application/json", tags = {"生产主体-主营产品-第三方抽检"})

public class ProductCheckRecordController {

    @Autowired
    ProductCheckRecordRepository productCheckRecordRepository;
    @Autowired
    ProductCheckRecordService productCheckRecordService;


    @GetMapping("/product_check_record/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductCheckRecord(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                 @RequestParam(defaultValue = "20") Integer pageSize,
                                                                 @RequestParam(defaultValue = "id") String sortBy,
                                                                 @RequestParam(defaultValue = "0") Integer product_id
    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductCheckRecord> list = productCheckRecordService.getAllProductCheckRecords(pageNo, pageSize, underscoreSortBy.toString(), product_id);

        List<ProductCheckRecord> totallist = productCheckRecordRepository.findAllBySearch(product_id);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);


        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/product_check_record/create")
    public ProductCheckRecord createProductCheckRecord(@Valid @RequestBody ProductCheckRecord productCheckRecord) {
        if (!productCheckRecordRepository.existsById(productCheckRecord.getId()))
            return productCheckRecordRepository.save(productCheckRecord);
        else
            return null;
    }


    @GetMapping("/product_check_record/get/{id}")
    public ProductCheckRecord getProductCheckRecord(@PathVariable(value = "id") Long id) {
        ProductCheckRecord productCheckRecord = productCheckRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCheckRecord", "id", id));
        return productCheckRecord;
    }

    @RequestMapping(value="/product_check_record/update/{id}", method = RequestMethod.PUT)
    public ProductCheckRecord updateProductCheckRecord(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody ProductCheckRecord updatedProductCheckRecord) {

        ProductCheckRecord productCheckRecord = productCheckRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCheckRecord", "id", id));


        productCheckRecord.setProductCheckTime(updatedProductCheckRecord.getProductCheckTime());
        productCheckRecord.setSpecimen(updatedProductCheckRecord.getSpecimen());
        productCheckRecord.setCheckItem(updatedProductCheckRecord.getCheckItem());
        productCheckRecord.setCheckResult(updatedProductCheckRecord.getCheckResult());
        productCheckRecord.setDetermine(updatedProductCheckRecord.getDetermine());
        productCheckRecord.setCheckStandard(updatedProductCheckRecord.getCheckStandard());
        productCheckRecord.setCheckOrganization(updatedProductCheckRecord.getCheckOrganization());
        productCheckRecord.setProductId(updatedProductCheckRecord.getProductId());

        productCheckRecord.setUpdateTime(updatedProductCheckRecord.getUpdateTime());
        productCheckRecord.setUpdateUserId(updatedProductCheckRecord.getUpdateUserId());

        ProductCheckRecord updateResult = productCheckRecordRepository.save(productCheckRecord);
        return updateResult;
    }


    @DeleteMapping("/product_check_record/delete/{id}")
    public ResponseEntity<?> deleteProductCheckRecord(@PathVariable(value = "id") Long id) {
        ProductCheckRecord productCheckRecord = productCheckRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCheckRecord", "id", id));

        productCheckRecordRepository.delete(productCheckRecord);

        return ResponseEntity.ok().build();
    }
}