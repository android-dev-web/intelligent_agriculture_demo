package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.QualityStandardService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.QualityStandard;
import com.springcloud.intelligentagriculture.model.SupervisionRecord;
import com.springcloud.intelligentagriculture.repository.QualityStandardRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "QualityStandard API", produces = "application/json", tags = {"三品一标"})

public class QualityStandardController {

    @Autowired
    QualityStandardRepository qualityStandardRepository;
    @Autowired
    QualityStandardService qualityStandardService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/quality_standard/all")
    @ResponseBody
    public ResponseEntity<String> getAllQualityStandards(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                        @RequestParam(defaultValue = "20") Integer pageSize,
                                                                        @RequestParam(defaultValue = "id") String sortBy,
                                                                        @RequestParam(defaultValue = "") String creditCode,
                                                                        @RequestParam(defaultValue = "0") String argriculturalClassification,
                                                                        @RequestParam(defaultValue = "0") String cretficationType  ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<QualityStandard> list = qualityStandardService.getAllQualityStandards(pageNo, pageSize, underscoreSortBy.toString(), creditCode, argriculturalClassification, cretficationType);

        List<QualityStandard> totallist = qualityStandardRepository.findAllBySearch(creditCode, argriculturalClassification, cretficationType);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);
        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/quality_standard/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public QualityStandard createQualityStandard(MultipartFile file, QualityStandard qualityStandard) {

        if(file != null)
        {
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            qualityStandard.setFiles(fileUploadUri);
        }

        if (!qualityStandardRepository.existsById(qualityStandard.getId()))
            return qualityStandardRepository.save(qualityStandard);
        else
            return null;
    }


    @GetMapping("/quality_standard/get/{id}")
    public QualityStandard getQualityStandardId(@PathVariable(value = "id") Long id) {
        QualityStandard qualityStandard = qualityStandardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("QualityStandard", "id", id));
        return qualityStandard;
    }

    @RequestMapping(value="/quality_standard/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public QualityStandard updateQualityStandard(@PathVariable(value = "id") Long id,
                                         QualityStandard qualityStandardDetails, MultipartFile file) {

        QualityStandard qualityStandard = qualityStandardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("QualityStandard", "id", id));

        if(file != null)
        {
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            qualityStandard.setFiles(fileUploadUri);
        }


        qualityStandard.setArgriculturalClassification(qualityStandardDetails.getArgriculturalClassification());
        qualityStandard.setCertificationEndTime(qualityStandardDetails.getCertificationEndTime());
        qualityStandard.setCertificationStartTime(qualityStandardDetails.getCertificationStartTime());
        qualityStandard.setCreateDate(qualityStandardDetails.getCreateDate());

        qualityStandard.setCreater(qualityStandardDetails.getCreater());
        qualityStandard.setCreditCode(qualityStandardDetails.getCreditCode());
        qualityStandard.setCretficationCategory(qualityStandardDetails.getCretficationCategory());
        qualityStandard.setCretficationType(qualityStandardDetails.getCretficationType());

        qualityStandard.setOutput(qualityStandardDetails.getOutput());
        qualityStandard.setProductId(qualityStandardDetails.getProductId());
        qualityStandard.setUpdateDate(qualityStandardDetails.getUpdateDate());
        qualityStandard.setUpdateUserId(qualityStandardDetails.getUpdateUserId());
        qualityStandard.setUpdater(qualityStandardDetails.getUpdater());
        qualityStandard.setCertificationNo(qualityStandardDetails.getCertificationNo());

        QualityStandard updatedistributor = qualityStandardRepository.save(qualityStandard);
        return updatedistributor;
    }


    @DeleteMapping("/quality_standard/delete/{id}")
    public ResponseEntity<?> deleteQualityStandard(@PathVariable(value = "id") Long id) {
        QualityStandard qualityStandard = qualityStandardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("QualityStandard", "id", id));

        qualityStandardRepository.delete(qualityStandard);

        return ResponseEntity.ok().build();
    }

}