package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.UserService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.Users;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.UserRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "User API", produces = "application/json", tags = {"用户管理"})

public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;


    @GetMapping("/user/all")
    @ResponseBody
    public ResponseEntity<String> getAllUsers(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy
                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<Users> list = userService.getAllUsers(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",userRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping("/user/create")
    public Users createUser(@Valid @RequestBody Users user) {

        Users addedUser = userRepository.findByUsername(user.getUserId());
        if( addedUser != null )
            return null;

        String password = user.getPassword();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        user.setPassword(hashedPassword);

        if (!userRepository.existsById(user.getId()))
            return userRepository.save(user);
        else
            return null;
    }


    @GetMapping("/user/get/{id}")
    public Users getUserId(@PathVariable(value = "id") Long id) {
        Users user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));
        return user;
    }

    @RequestMapping(value="/user/update/{id}", method = RequestMethod.PUT)
    public Users updateUser(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody Users updatedUser) {

        Users user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));


        user.setUserId(updatedUser.getUserId());
        user.setContactName(updatedUser.getContactName());
        user.setUserType(updatedUser.getUserType());
        user.setContactPerson(updatedUser.getContactPerson());
        user.setContactWay(updatedUser.getContactWay());
        user.setCreditCode(updatedUser.getCreditCode());

        user.setUpdateTime(updatedUser.getUpdateTime());
        user.setUpdateUserId(updatedUser.getUpdateUserId());

        String password = updatedUser.getPassword();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        user.setPassword(hashedPassword);

        Users updateResult = userRepository.save(user);
        return updateResult;
    }


    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long id) {
        Users user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));

        userRepository.delete(user);

        return ResponseEntity.ok().build();
    }


    @PostMapping("/user/changePassword")
    public Users changePassword(@RequestParam String userId, @RequestParam String oldPassword, String newPassword) {

        Users user = userRepository.findByUsername(userId);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if( passwordEncoder.matches(oldPassword, user.getPassword()))
        {
            user.setPassword(passwordEncoder.encode(newPassword));
            return  userRepository.save(user);
        }
        else
            return null;
    }
}