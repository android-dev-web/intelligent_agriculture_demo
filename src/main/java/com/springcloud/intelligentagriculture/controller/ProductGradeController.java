package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductGradeService;
import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductGrade;
import com.springcloud.intelligentagriculture.model.QualityStandard;
import com.springcloud.intelligentagriculture.model.SupervisionRecord;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.ProductGradeRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductGrade API", produces = "application/json", tags = {"生产主体/主营产品/定义等级"})

public class ProductGradeController {

    @Autowired
    ProductGradeRepository productGradeRepository;
    @Autowired
    ProductGradeService productGradeService;

    @GetMapping("/product_grade/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductGrade(@RequestParam(defaultValue = "0") Integer pageNo,
                                                        @RequestParam(defaultValue = "20") Integer pageSize,
                                                        @RequestParam(defaultValue = "id") String sortBy,
                                                        @RequestParam(defaultValue = "0") Long productId

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductGrade> list = productGradeService.getAllProductGrade(pageNo, pageSize, underscoreSortBy.toString(), productId);

        List<ProductGrade> totallist = productGradeRepository.findAllBySearch(productId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/product_grade/get/{id}")
    public ProductGrade getProductGrade(@PathVariable(value = "id") Long id) {
        ProductGrade productGrade = productGradeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductGrade", "id", id));
        return productGrade;
    }

    @PostMapping("/product_grade/create")
    public ProductGrade createProductGrade(@Valid @RequestBody ProductGrade productGrade) {

        if (!productGradeRepository.existsById(productGrade.getId()))
            return productGradeRepository.save(productGrade);
        else
            return null;
    }

    @RequestMapping(value="/product_grade/update/{id}", method = RequestMethod.PUT)
    public ProductGrade updateProductGrade(@PathVariable(value = "id") Long id,
                                                 @Valid @RequestBody ProductGrade updatedProductGrade) {

        ProductGrade productGrade = productGradeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductGrade", "id", id));

        productGrade.setGradeName(updatedProductGrade.getGradeName());
        productGrade.setGradeSort(updatedProductGrade.getGradeSort());
        productGrade.setDoShare(updatedProductGrade.getDoShare());
        productGrade.setProductId(updatedProductGrade.getProductId());

        productGrade.setUpdateTime(updatedProductGrade.getUpdateTime());
        productGrade.setUpdateUserId(updatedProductGrade.getUpdateUserId());

        ProductGrade updateResult = productGradeRepository.save(productGrade);
        return updateResult;
    }


    @DeleteMapping("/product_grade/delete/{id}")
    public ResponseEntity<?> deleteProductGrade(@PathVariable(value = "id") Long id) {
        ProductGrade productGrade = productGradeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductGrade", "id", id));

        productGradeRepository.delete(productGrade);

        return ResponseEntity.ok().build();
    }
}