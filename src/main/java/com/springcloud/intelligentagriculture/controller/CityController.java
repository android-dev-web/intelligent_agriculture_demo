package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.City;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.CityRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "City API", produces = "application/json", tags = {"市，区"})

public class CityController {

    @Autowired
    CityRepository cityRepository;


    @GetMapping("/city/all")
    @ResponseBody
    public ResponseEntity<List<City>> getAllCity( @RequestParam(defaultValue = "0") Long provinceId) {

        List<City> list = cityRepository.findAllBySearch(provinceId);

        return new ResponseEntity<List<City>>(list, new HttpHeaders(), HttpStatus.OK);
    }
}