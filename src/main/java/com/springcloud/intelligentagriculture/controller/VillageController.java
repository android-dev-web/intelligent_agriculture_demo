package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.City;
import com.springcloud.intelligentagriculture.model.Village;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.CityRepository;
import com.springcloud.intelligentagriculture.repository.VillageRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "Village API", produces = "application/json", tags = {"村"})

public class VillageController {

    @Autowired
    VillageRepository villageRepository;


    @GetMapping("/village/all")
    @ResponseBody
    public ResponseEntity<List<Village>> getAllVillage(@RequestParam(defaultValue = "0") Long provinceId,
                                                              @RequestParam(defaultValue = "0") Long cityId,
                                                              @RequestParam(defaultValue = "0") Long countryId) {

        List<Village> list = villageRepository.findAllBySearch(provinceId, cityId, countryId);

        return new ResponseEntity<List<Village>>(list, new HttpHeaders(), HttpStatus.OK);
    }
}