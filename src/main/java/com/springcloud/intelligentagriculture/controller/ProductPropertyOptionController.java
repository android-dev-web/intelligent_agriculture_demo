package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.CommonWordService;
import com.springcloud.intelligentagriculture.Service.ProductPropertyOptionService;
import com.springcloud.intelligentagriculture.Service.ProductPropertyService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.CommonWordRepository;
import com.springcloud.intelligentagriculture.repository.ProductPropertyOptionRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductPropertyOption API", produces = "application/json", tags = {"ProductPropertyOption"})

public class ProductPropertyOptionController {

    @Autowired
    ProductPropertyOptionRepository productPropertyOptionRepository;

    @Autowired
    ProductPropertyOptionService productPropertyOptionService;


    @GetMapping("/product_property_option/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductPropertyOption(@RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                                    @RequestParam(defaultValue = "id") String sortBy) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductPropertyOption> list = productPropertyOptionService.getAllProductPropertyOption(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",productPropertyOptionRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @DeleteMapping("/product_property_option/delete/{id}")
    public ResponseEntity<?> deleteProductPropertyOption(@PathVariable(value = "id") Long id) {
        ProductPropertyOption productPropertyOption = productPropertyOptionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductPropertyOption", "id", id));

        productPropertyOptionRepository.delete(productPropertyOption);

        return ResponseEntity.ok().build();
    }


    @PostMapping("/product_property_option/create")
    public ProductPropertyOption createProductPropertyOption(@Valid @RequestBody ProductPropertyOption productPropertyOption) {

        if (!productPropertyOptionRepository.existsById(productPropertyOption.getId()))
            return productPropertyOptionRepository.save(productPropertyOption);
        else
            return null;
    }


    @PutMapping("/product_property_option/update/{id}")
    public ProductPropertyOption updateProductPropertyOption(@PathVariable(value = "id") Long id,
                                       @Valid @RequestBody ProductPropertyOption updatedProductPropertyOption) {

        ProductPropertyOption productPropertyOption = productPropertyOptionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductPropertyOption", "id", id));


        productPropertyOption.setName(updatedProductPropertyOption.getName());
        productPropertyOption.setSort(updatedProductPropertyOption.getSort());

        ProductPropertyOption updatedistributor = productPropertyOptionRepository.save(productPropertyOption);
        return updatedistributor;
    }

}