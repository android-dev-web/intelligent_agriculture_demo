package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "Blacklist API", produces = "application/json", tags = {"红黑名单"})

public class BlacklistController {

    @Autowired
    BlacklistRepository blacklistRepository;
    @Autowired
    BlacklistService blacklistService;


    @GetMapping("/blacklist/all")
    @ResponseBody
    public ResponseEntity<String> getAllBlacklistTypes(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                @RequestParam(defaultValue = "20") Integer pageSize,
                                                                @RequestParam(defaultValue = "id") String sortBy,
                                                                @RequestParam(defaultValue = "0") Integer blacklistType,
                                                                @RequestParam(defaultValue = "0") Integer townId) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<Blacklist> list = blacklistService.getAllBlacklists(pageNo, pageSize, underscoreSortBy.toString(), blacklistType, townId);

        List<Blacklist> totallist = blacklistRepository.findAllBy(blacklistType, townId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }
}