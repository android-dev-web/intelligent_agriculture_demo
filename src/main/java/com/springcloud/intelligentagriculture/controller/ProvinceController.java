package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.TownService;
import com.springcloud.intelligentagriculture.model.Province;
import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.repository.ProvinceRepository;
import com.springcloud.intelligentagriculture.repository.TownRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "Province API", produces = "application/json", tags = {"省"})
public class ProvinceController {

    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/province/all")
    @ResponseBody
    public ResponseEntity<List<Province>> getAllProvince() {

        List<Province> list = provinceRepository.findAll();
        return new ResponseEntity<List<Province>>(list, new HttpHeaders(), HttpStatus.OK);
    }


}
