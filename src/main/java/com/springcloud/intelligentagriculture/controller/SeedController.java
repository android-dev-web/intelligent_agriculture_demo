package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.SeedService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.Seed;
import com.springcloud.intelligentagriculture.model.SeedField;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.SeedRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")

@Api(description = "Seed API", produces = "application/json", tags = {"种子管理"})

public class SeedController {

    @Autowired
    SeedRepository seedRepository;
    @Autowired
    SeedService seedService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/seed/all")
    @ResponseBody
    public ResponseEntity<String> getAllSeeds(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy
                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<Seed> list = seedService.getAllSeeds(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",seedRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value="/seed/create", consumes={"multipart/form-data"})
    public Seed createSeed(MultipartFile file, Seed seed) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            seed.setSeedProfiles(fileUploadUri);
        }
        if (!seedRepository.existsById(seed.getId()))
            return seedRepository.save(seed);
        else
            return null;
    }


    @GetMapping("/seed/get/{id}")
    public Seed getSeedId(@PathVariable(value = "id") Long id) {
        Seed seed = seedRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Seed", "id", id));
        return seed;
    }

    @RequestMapping(value="/seed/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public Seed updateSeed(MultipartFile file, @PathVariable(value = "id") Long id,
                                     Seed updatedSeed) {

        Seed seed = seedRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Seed", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            seed.setSeedProfiles(fileUploadUri);
        }

        seed.setSeedProfiles(updatedSeed.getSeedProfiles());
        seed.setVillageId(updatedSeed.getVillageId());
        seed.setTownId(updatedSeed.getTownId());
        seed.setCityId(updatedSeed.getCityId());
        seed.setCompanyId(updatedSeed.getCompanyId());
        seed.setUpdateTime(updatedSeed.getUpdateTime());
        seed.setUpdateUserId(updatedSeed.getUpdateUserId());

        Seed updateResult = seedRepository.save(seed);
        return updateResult;
    }


    @DeleteMapping("/seed/delete/{id}")
    public ResponseEntity<?> deleteSeed(@PathVariable(value = "id") Long id) {
        Seed seed = seedRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Seed", "id", id));

        seedRepository.delete(seed);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/seed/profile/{id}")
    public ResponseEntity<String> uploadFile(@PathVariable(value = "id") Long id, HttpServletRequest request) {

        Seed seed = seedRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Seed", "id", id));

        String fileName = seed.getSeedProfiles();
        String filePath = "/app" + fileName;
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(filePath);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        List<SeedField> tempFieldList = new ArrayList<SeedField>();
        XSSFWorkbook workbook = null;
        try {
            workbook = new XSSFWorkbook(resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {
            SeedField tempField = new SeedField();

            XSSFRow row = worksheet.getRow(i);

            tempField.setField1(row.getCell(0).getStringCellValue());
            tempField.setField2(row.getCell(1).getStringCellValue());
            tempField.setField3(row.getCell(2).getRawValue());
            tempField.setField4(row.getCell(3).getRawValue());
            tempField.setField5(row.getCell(4).getRawValue());
            tempField.setField6(row.getCell(5).getRawValue());
            tempField.setField7(row.getCell(6).getRawValue());
            tempField.setField8(row.getCell(7).getRawValue());
            tempFieldList.add(tempField);
        }

        JSONObject entity = new JSONObject();
        entity.put("total",worksheet.getPhysicalNumberOfRows() - 1);
        entity.put("data",tempFieldList);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }
}