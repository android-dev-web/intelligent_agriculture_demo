package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductionStandardService;
import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductionStandard;
import com.springcloud.intelligentagriculture.model.QualityStandard;
import com.springcloud.intelligentagriculture.model.SupervisionRecord;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.ProductionStandardRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductionStandard API", produces = "application/json", tags = {"生产标准"})

public class ProductionStandardController {

    @Autowired
    ProductionStandardRepository productionStandardRepository;
    @Autowired
    ProductionStandardService productionStandardService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/production_standard/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductionStandard(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(defaultValue = "") String productName,
                                                            @RequestParam(defaultValue = "0") String category

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductionStandard> list = productionStandardService.getAllProductionStandard(pageNo, pageSize, underscoreSortBy.toString(), productName, category);

        List<ProductionStandard> totallist = productionStandardRepository.findAllBySearch(productName, category);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/production_standard/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ProductionStandard createProductionStandard(MultipartFile file,ProductionStandard productionStandard ) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productionStandard.setProductionStandardProfiles(fileUploadUri);
        }


        if (!productionStandardRepository.existsById(productionStandard.getId()))
            return productionStandardRepository.save(productionStandard);
        else
            return null;
    }

    @RequestMapping(value="/production_standard/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public ProductionStandard updateProductionStandard(@PathVariable(value = "id") Long id, MultipartFile file,
                                           ProductionStandard updatedProductionStandard) {

        ProductionStandard productionStandard = productionStandardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductionStandard", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productionStandard.setProductionStandardProfiles(fileUploadUri);
        }
        productionStandard.setProductName(updatedProductionStandard.getProductName());
        productionStandard.setCategory(updatedProductionStandard.getCategory());
        productionStandard.setReleaseTime(updatedProductionStandard.getReleaseTime());
        productionStandard.setReleasePerson(updatedProductionStandard.getReleasePerson());
        productionStandard.setProductStandard(updatedProductionStandard.getProductStandard());
        productionStandard.setProductionStandardProfiles(updatedProductionStandard.getProductionStandardProfiles());
        productionStandard.setProductId(updatedProductionStandard.getProductId());

        productionStandard.setUpdateTime(updatedProductionStandard.getUpdateTime());
        productionStandard.setUpdateUserId(updatedProductionStandard.getUpdateUserId());

        ProductionStandard updateResult = productionStandardRepository.save(productionStandard);
        return updateResult;
    }
    @GetMapping("/production_standard/get/{id}")
    public ProductionStandard getProductionStandard(@PathVariable(value = "id") Long id) {
        ProductionStandard productionStandard = productionStandardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductionStandard", "id", id));
        return productionStandard;
    }
}