package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductPropertyService;
import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductProperty;
import com.springcloud.intelligentagriculture.model.QualityStandard;
import com.springcloud.intelligentagriculture.model.SupervisionRecord;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.ProductPropertyRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductProperty API", produces = "application/json", tags = {"生产主体/主营产品/属性管理"})

public class ProductPropertyController {

    @Autowired
    ProductPropertyRepository productPropertyRepository;
    @Autowired
    ProductPropertyService productPropertyService;

    @GetMapping("/product_property/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductProperty(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(defaultValue = "0") Long productId


    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductProperty> list = productPropertyService.getAllProductProperty(pageNo, pageSize, underscoreSortBy.toString(), productId);

        List<ProductProperty> totallist = productPropertyRepository.findAllBySearch(productId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/product_property/get/{id}")
    public ProductProperty getProductProperty(@PathVariable(value = "id") Long id) {
        ProductProperty productProperty = productPropertyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductProperty", "id", id));
        return productProperty;
    }

    @PostMapping("/product_property/create")
    public ProductProperty createProductProperty(@Valid @RequestBody ProductProperty productProperty) {

        if (!productPropertyRepository.existsById(productProperty.getId()))
            return productPropertyRepository.save(productProperty);
        else
            return null;
    }

    @RequestMapping(value="/product_property/update/{id}", method = RequestMethod.PUT)
    public ProductProperty updateProductProperty(@PathVariable(value = "id") Long id,
                           @Valid @RequestBody ProductProperty updatedProductProperty) {

        ProductProperty productProperty = productPropertyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductProperty", "id", id));

        productProperty.setPropertyName(updatedProductProperty.getPropertyName());
        productProperty.setPropertyOptions(updatedProductProperty.getPropertyOptions());
        productProperty.setPropertySort(updatedProductProperty.getPropertySort());
        productProperty.setProductId(updatedProductProperty.getProductId());
        productProperty.setDoShare(updatedProductProperty.getDoShare());
        productProperty.setUpdateTime(updatedProductProperty.getUpdateTime());
        productProperty.setUpdateUserId(updatedProductProperty.getUpdateUserId());

        ProductProperty updateResult = productPropertyRepository.save(productProperty);
        return updateResult;
    }


    @DeleteMapping("/product_property/delete/{id}")
    public ResponseEntity<?> deleteProductProperty(@PathVariable(value = "id") Long id) {
        ProductProperty productProperty = productPropertyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductProperty", "id", id));

        productPropertyRepository.delete(productProperty);

        return ResponseEntity.ok().build();
    }
}