package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.SampleCheckResultService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.SampleCheck;
import com.springcloud.intelligentagriculture.model.SampleCheckResult;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckResultRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "SampleCheckResult API", produces = "application/json", tags = {"例行抽样/抽样结果"})

public class SampleCheckResultController {

    @Autowired
    SampleCheckResultRepository sampleCheckResultRepository;
    @Autowired
    SampleCheckResultService sampleCheckResultService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/sample_check_result/all")
    @ResponseBody
    public ResponseEntity<String> getAllSampleCheckResult(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                           @RequestParam(defaultValue = "20") Integer pageSize,
                                                                           @RequestParam(defaultValue = "id") String sortBy

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<SampleCheckResult> list = sampleCheckResultService.getAllSampleCheckResult(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",sampleCheckResultRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/sample_check_result/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public SampleCheckResult createSampleCheckResult(MultipartFile file, SampleCheckResult sampleCheckResult ) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            sampleCheckResult.setCheckFiles(fileUploadUri);
        }

        if (!sampleCheckResultRepository.existsById(sampleCheckResult.getId()))
            return sampleCheckResultRepository.save(sampleCheckResult);
        else
            return null;
    }

    @GetMapping("/sample_check_result/get/{id}")
    public SampleCheckResult getSampleCheckResult(@PathVariable(value = "id") Long id) {

        SampleCheckResult sampleCheckResult = sampleCheckResultRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("SampleCheckResult", "id", id));;

        return sampleCheckResult;
    }
}