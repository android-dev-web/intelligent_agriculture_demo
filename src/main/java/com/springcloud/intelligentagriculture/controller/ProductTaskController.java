package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.*;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.*;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductTask API", produces = "application/json", tags = {"生产主体/主营产品/作业定义"})

public class ProductTaskController {

    @Autowired
    ProductTaskRepository productTaskRepository;
    @Autowired
    ProductTaskService productTaskService;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/product_task/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductTask(@RequestParam(defaultValue = "0") Integer pageNo,
                                                     @RequestParam(defaultValue = "20") Integer pageSize,
                                                     @RequestParam(defaultValue = "id") String sortBy,
                                                     @RequestParam(defaultValue = "0") Long productId


    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductTask> list = productTaskService.getAllProductTask(pageNo, pageSize, underscoreSortBy.toString(), productId);

        List<ProductTask> totallist = productTaskRepository.findAllBySearch(productId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }



    @RequestMapping(value="/product_task/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ProductTask createProductTask(MultipartFile file,  ProductTask productTask ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productTask.setTaskImages(fileUploadUri);
        }

        if (!productTaskRepository.existsById(productTask.getId()))
            return productTaskRepository.save(productTask);
        else
            return null;
    }

    @RequestMapping(value="/product_task/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public ProductTask updateProductTask(MultipartFile file, @PathVariable(value = "id") Long id, ProductTask updatedProductTask )
    {

        ProductTask productTask = productTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductTask", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            productTask.setTaskImages(fileUploadUri);
        }

        productTask.setTaskName(updatedProductTask.getTaskName());
        productTask.setTaskType(updatedProductTask.getTaskType());
        productTask.setProductId(updatedProductTask.getProductId());
        productTask.setDoShare(updatedProductTask.getDoShare());

        productTask.setUpdateTime(updatedProductTask.getUpdateTime());
        productTask.setUpdateUserId(updatedProductTask.getUpdateUserId());

        ProductTask updatedResult = productTaskRepository.save(productTask);
        return updatedResult;
    }


    @GetMapping("/product_task/get/{id}")
    public ProductTask getProductTask(@PathVariable(value = "id") Long id) {

        ProductTask productTask = productTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductTask", "id", id));;

        return productTask;
    }


    @DeleteMapping("/product_task/delete/{id}")
    public ResponseEntity<?> deleteProductTask(@PathVariable(value = "id") Long id) {
        ProductTask productTask = productTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductTask", "id", id));

        productTaskRepository.delete(productTask);

        return ResponseEntity.ok().build();
    }

}