package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.ProductRepetoryService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.ProductRepetory;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.ProductRepetoryRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductRepertory API", produces = "application/json", tags = {"生产主体-主营产品-库存动态"})

public class ProductRepetoryController {

    @Autowired
    ProductRepetoryRepository productRepetoryRepository;
    @Autowired
    ProductRepetoryService productRepetoryService;


    @GetMapping("/product_repetory/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductRepertorys(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                         @RequestParam(defaultValue = "20") Integer pageSize,
                                                                         @RequestParam(defaultValue = "id") String sortBy,
                                                                         @RequestParam(defaultValue = "0") Integer product_id
    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductRepetory> list = productRepetoryService.getAllProductRepetorys(pageNo, pageSize, underscoreSortBy.toString(), product_id);

        List<ProductRepetory> totallist = productRepetoryRepository.findAllBySearch(product_id);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/product_repetory/create")
    public ProductRepetory registerProductRepetory(@Valid @RequestBody ProductRepetory productRepetory) {
        if (!productRepetoryRepository.existsById(productRepetory.getId()))
            return productRepetoryRepository.save(productRepetory);
        else
            return null;
    }


    @GetMapping("/product_repetory/get/{id}")
    public ProductRepetory getProductRepetoryId(@PathVariable(value = "id") Long id) {
        ProductRepetory productRepetory = productRepetoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductRepetory", "id", id));
        return productRepetory;
    }

    @RequestMapping(value="/product_repetory/update/{id}", method = RequestMethod.PUT)
    public ProductRepetory updateProductRepetory(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody ProductRepetory updatedProductRepetory) {

        ProductRepetory productRepetory = productRepetoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductRepetory", "id", id));



        productRepetory.setGrade(updatedProductRepetory.getGrade());
        productRepetory.setProductId(updatedProductRepetory.getProductId());
        productRepetory.setRepertoryAmount(updatedProductRepetory.getRepertoryAmount());
        productRepetory.setUpdateTime(updatedProductRepetory.getUpdateTime());
        productRepetory.setUpdateUserId(updatedProductRepetory.getUpdateUserId());
        productRepetory.setVariety(updatedProductRepetory.getVariety());
        productRepetory.setWarehouseId(updatedProductRepetory.getWarehouseId());


        ProductRepetory updateResult = productRepetoryRepository.save(productRepetory);
        return updateResult;
    }


    @DeleteMapping("/product_repetory/delete/{id}")
    public ResponseEntity<?> deleteProductRepetory(@PathVariable(value = "id") Long id) {
        ProductRepetory productRepetory = productRepetoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductRepetory", "id", id));

        productRepetoryRepository.delete(productRepetory);

        return ResponseEntity.ok().build();
    }
}