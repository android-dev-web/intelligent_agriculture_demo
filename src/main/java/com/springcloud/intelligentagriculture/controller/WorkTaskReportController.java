package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.JobDefinitionService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.Service.WorkTaskReportService;
import com.springcloud.intelligentagriculture.Service.WorkTaskService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.JobDefinitionRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.repository.WorkTaskReportRepository;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "WorkTaskReport API", produces = "application/json", tags = {"工作任务汇报"})

public class WorkTaskReportController {

    @Autowired
    WorkTaskReportRepository workTaskReportRepository;
    @Autowired
    WorkTaskReportService workTaskReportService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/work_task_report/all")
    @ResponseBody
    public ResponseEntity<String> getAllWorkTaskReport(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                     @RequestParam(defaultValue = "20") Integer pageSize,
                                                                     @RequestParam(defaultValue = "id") String sortBy,
                                                                     @RequestParam(defaultValue = "0") Long workTaskId

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<WorkTaskReport> list = workTaskReportService.getAllWorkTaskReport(pageNo, pageSize, underscoreSortBy.toString(), workTaskId);

        List<WorkTaskReport> totallist = workTaskReportRepository.findAllBySearch(workTaskId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/work_task_report/get/{id}")
    public WorkTaskReport getWorkTaskReport(@PathVariable(value = "id") Long id) {

        WorkTaskReport workTaskReport = workTaskReportRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("WorkTaskReport", "id", id));;

        return workTaskReport;
    }

    @RequestMapping(value="/work_task_report/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public WorkTaskReport createWorkTaskReport(MultipartFile file,  WorkTaskReport workTaskReport ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file, "work_task_report");
            String fileUploadUri = "/uploads/work_task_report/" + fileName.toString();
            workTaskReport.setReportProfiles(fileUploadUri);
        }

        if (!workTaskReportRepository.existsById(workTaskReport.getId()))
            return workTaskReportRepository.save(workTaskReport);
        else
            return null;
    }


}