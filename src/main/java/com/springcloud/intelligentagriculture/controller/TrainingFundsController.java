package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.TrainingFundsService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.TrainingFunds;
import com.springcloud.intelligentagriculture.repository.TrainingFundsRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "TrainingFunds API", produces = "application/json", tags = {"培训经费管理"})

public class TrainingFundsController {

    @Autowired
    TrainingFundsRepository trainingFundsRepository;
    @Autowired
    TrainingFundsService trainingFundsService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/training_funds/all")
    @ResponseBody
    public ResponseEntity<String> getAllTrainingFunds(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                      @RequestParam(defaultValue = "-1") Integer specialFlag,
                                                      @RequestParam(defaultValue = "") String createTime,
                                                      @RequestParam(defaultValue = "") String createTimeFrom,
                                                      @RequestParam(defaultValue = "") String createTimeTo
                                                                ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<TrainingFunds> list = trainingFundsService.getAllTrainingFunds(pageNo, pageSize, underscoreSortBy.toString(), specialFlag, createTime, createTimeFrom, createTimeTo);
        List<TrainingFunds> totalList = trainingFundsRepository.findAllBySearch(specialFlag, createTime, createTimeFrom, createTimeTo);
//        List<TrainingFunds> totalList = trainingFundsRepository.findAllBySearch(specialFlag);

        JSONObject entity = new JSONObject();
        entity.put("total",totalList.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value="/training_funds/create", consumes = {"multipart/form-data"})
    public TrainingFunds createTrainingFunds(MultipartFile file, TrainingFunds trainingFunds) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            trainingFunds.setTrainingFundsProfiles(fileUploadUri);
        }
        if (!trainingFundsRepository.existsById(trainingFunds.getId()))
            return trainingFundsRepository.save(trainingFunds);
        else
            return null;
    }

    @GetMapping("/training_funds/get/{id}")
    public TrainingFunds getTrainingFundsId(@PathVariable(value = "id") Long id) {
        TrainingFunds trainingFunds = trainingFundsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TrainingFunds", "id", id));
        return trainingFunds;
    }

    @PutMapping(value="/training_funds/update/{id}",consumes = {"multipart/form-data"})
    public TrainingFunds updateTrainingFunds(MultipartFile file, @PathVariable(value = "id") Long id,
                                     TrainingFunds updatedTrainingFunds) {

        TrainingFunds trainingFunds = trainingFundsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TrainingFunds", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            trainingFunds.setTrainingFundsProfiles(fileUploadUri);
        }

        trainingFunds.setId(updatedTrainingFunds.getId());
        trainingFunds.setTrainingFundsProfiles(updatedTrainingFunds.getTrainingFundsProfiles());
        trainingFunds.setProjectName(updatedTrainingFunds.getProjectName());
        trainingFunds.setAppliedAmount(updatedTrainingFunds.getAppliedAmount());
        trainingFunds.setProposer(updatedTrainingFunds.getProposer());
        trainingFunds.setCompanyId(updatedTrainingFunds.getCompanyId());
        trainingFunds.setTrainingFundsProfiles(updatedTrainingFunds.getTrainingFundsProfiles());
        trainingFunds.setUpdateTime(updatedTrainingFunds.getUpdateTime());
        trainingFunds.setUpdateUserId(updatedTrainingFunds.getUpdateUserId());

        TrainingFunds updateResult = trainingFundsRepository.save(trainingFunds);
        return updateResult;
    }

    @PutMapping(value="/training_funds/update_special_flag/{id}")
    public TrainingFunds updateTrainingFunds(@PathVariable(value = "id") Long id, @PathVariable(value = "specialFlag")int special_flag) {

        TrainingFunds trainingFunds = trainingFundsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TrainingFunds", "id", id));

        trainingFunds.setSpecialFlag(special_flag);

        TrainingFunds updateResult = trainingFundsRepository.save(trainingFunds);
        return updateResult;
    }


    @DeleteMapping("/training_funds/delete/{id}")
    public ResponseEntity<?> deleteTrainingFunds(@PathVariable(value = "id") Long id) {
        TrainingFunds trainingFunds = trainingFundsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TrainingFunds", "id", id));

        trainingFundsRepository.delete(trainingFunds);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/training_funds/profile/{id}")
    public ResponseEntity<Resource> uploadFile(@PathVariable(value = "id") Long id, HttpServletRequest request) {

        TrainingFunds trainingFunds = trainingFundsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TrainingFunds", "id", id));

        String fileName = trainingFunds.getTrainingFundsProfiles();
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}