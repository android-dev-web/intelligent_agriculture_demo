package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.BlacklistService;
import com.springcloud.intelligentagriculture.Service.ShareFilesRecordService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.ShareFilesRecord;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.ShareFilesRecordRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")

@Api(description = "ShareFilesRecord API", produces = "application/json", tags = {"共享文件/会议文件"})

public class ShareFilesRecordController {

    @Autowired
    ShareFilesRecordRepository shareFilesRecordRepository;
    @Autowired
    ShareFilesRecordService shareFilesRecordService;

    private static final Logger logger = LoggerFactory.getLogger(FileDownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/shareFilesRecord/all")
    @ResponseBody
    public ResponseEntity<String> getAllShareFilesRecords(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(defaultValue = "-1") Integer shareFilesId,
                                                            @RequestParam(defaultValue = "") String uploadCompany
                                                          ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ShareFilesRecord> list = shareFilesRecordService.getAllShareFilesRecords(pageNo, pageSize, underscoreSortBy.toString(), shareFilesId, uploadCompany);

        List<ShareFilesRecord> totalList = shareFilesRecordRepository.findAllBySearch(shareFilesId, uploadCompany);

        JSONObject entity = new JSONObject();
        entity.put("total",totalList.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value="/shareFilesRecord/create", consumes={"multipart/form-data"})
    public ShareFilesRecord createShareFilesRecord(MultipartFile file, ShareFilesRecord shareFilesRecord) {

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            shareFilesRecord.setUploadProfile(fileUploadUri);
        }

        if (!shareFilesRecordRepository.existsById(shareFilesRecord.getId()))
            return shareFilesRecordRepository.save(shareFilesRecord);
        else
            return null;
    }


    @GetMapping("/shareFilesRecord/get/{id}")
    public ShareFilesRecord getShareFilesRecordId(@PathVariable(value = "id") Long id) {
        ShareFilesRecord shareFilesRecord = shareFilesRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFilesRecord", "id", id));
        return shareFilesRecord;
    }

    @RequestMapping(value="/shareFilesRecord/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public ShareFilesRecord updateShareFilesRecord(MultipartFile file, @PathVariable(value = "id") Long id,
                                     ShareFilesRecord updatedShareFilesRecord) {

        ShareFilesRecord shareFilesRecord = shareFilesRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFilesRecord", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            shareFilesRecord.setUploadProfile(fileUploadUri);
        }

        shareFilesRecord.setShareFilesId(updatedShareFilesRecord.getShareFilesId());
        shareFilesRecord.setUploadCompany(updatedShareFilesRecord.getUploadCompany());
        shareFilesRecord.setUploadPerson(updatedShareFilesRecord.getUploadPerson());
        shareFilesRecord.setUploadProfile(updatedShareFilesRecord.getUploadProfile());

        shareFilesRecord.setUpdateTime(updatedShareFilesRecord.getUpdateTime());
        shareFilesRecord.setUpdateUserId(updatedShareFilesRecord.getUpdateUserId());

        ShareFilesRecord updateResult = shareFilesRecordRepository.save(shareFilesRecord);
        return updateResult;
    }


    @DeleteMapping("/shareFilesRecord/delete/{id}")
    public ResponseEntity<?> deleteShareFilesRecord(@PathVariable(value = "id") Long id) {
        ShareFilesRecord shareFilesRecord = shareFilesRecordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ShareFilesRecord", "id", id));

        shareFilesRecordRepository.delete(shareFilesRecord);

        return ResponseEntity.ok().build();
    }

}