package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.CompanyProductionService;
import com.springcloud.intelligentagriculture.Service.CompanyCreditGradeService;

import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.CompanyCreditGrade;
import com.springcloud.intelligentagriculture.model.CompanyProduction;
import com.springcloud.intelligentagriculture.model.Notice;
import com.springcloud.intelligentagriculture.model.WorkTaskReport;
import com.springcloud.intelligentagriculture.repository.CompanyProductionRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "CompanyProduction API", produces = "application/json", tags = {"生产主体"})
public class CompanyProductionController {

    @Autowired
    CompanyProductionService companyProductionService;
    @Autowired
    CompanyProductionRepository companyProductionRepository;

    @GetMapping("/company_production/name")
    @ResponseBody
    public ResponseEntity<List<CompanyProduction>> getNames(    @RequestParam(defaultValue = "-1") String creditCode,
                                                                @RequestParam(defaultValue = "0") Integer companyid )  {

        List<CompanyProduction> list = companyProductionService.getNameCompanyProductions(creditCode, companyid);
        return new ResponseEntity<List<CompanyProduction>>(list, new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping("/company_production/create")
    public CompanyProduction registerGrade(@Valid @RequestBody CompanyProduction companyProduction) {
        if (!companyProductionRepository.existsById(companyProduction.getCompanyId()))
            return companyProductionRepository.save(companyProduction);
        else
            return null;
    }

    @GetMapping("/company_production/all")
    @ResponseBody
    public ResponseEntity<String> CompanyProduction(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                            @RequestParam(defaultValue = "20") Integer pageSize,
                                                                            @RequestParam(defaultValue = "companyId") String sortBy,
                                                                             @RequestParam(defaultValue = "0") Integer townId,
                                                                             @RequestParam(defaultValue = "0") String companyType

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<CompanyProduction> list = companyProductionService.getAllCompanyProductions(pageNo, pageSize, underscoreSortBy.toString(), townId, companyType);

        List<CompanyProduction> totalList = companyProductionRepository.findAllCompanyProductions(townId, companyType);

        JSONObject entity = new JSONObject();
        entity.put("total",totalList.size());
        entity.put("data",list);

        for(int i = 0 ; i < list.size(); i++)
        {
            List<CompanyCreditGrade> listCreditGrade = companyProductionService.getAllCreditGradesByCreditCode(list.get(i).getCreditCode());
            String key = "credit_grade_data_" + String.valueOf(i);
            entity.put(key, listCreditGrade);
        }

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/company_production/update/{id}", method = RequestMethod.PUT)
    public CompanyProduction updateCompanyProduction(@PathVariable(value = "id") Long id, @Valid @RequestBody CompanyProduction updatedCompanyProduction )
    {

        CompanyProduction companyProduction = companyProductionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyProduction", "id", id));


        companyProduction.setCreditCode(updatedCompanyProduction.getCreditCode());
        companyProduction.setCompanyType(updatedCompanyProduction.getCompanyType());
        companyProduction.setTownId(updatedCompanyProduction.getTownId());
        companyProduction.setAgriculturalClassification(updatedCompanyProduction.getAgriculturalClassification());
        companyProduction.setCompanyName(updatedCompanyProduction.getCompanyName());
        companyProduction.setChargePerson(updatedCompanyProduction.getChargePerson());
        companyProduction.setContactWay(updatedCompanyProduction.getContactWay());
        companyProduction.setPlantArea(updatedCompanyProduction.getPlantArea());
        companyProduction.setLandSource(updatedCompanyProduction.getLandSource());
        companyProduction.setCompanyAddress(updatedCompanyProduction.getCompanyAddress());
        companyProduction.setProductInfo(updatedCompanyProduction.getProductInfo());
        companyProduction.setQualityStandardId(updatedCompanyProduction.getQualityStandardId());
        companyProduction.setContactPerson(updatedCompanyProduction.getContactPerson());
        companyProduction.setContactMobile(updatedCompanyProduction.getContactMobile());
        companyProduction.setCompanyHonor(updatedCompanyProduction.getCompanyHonor());
        companyProduction.setRemarks(updatedCompanyProduction.getRemarks());
        companyProduction.setDoSupervision(updatedCompanyProduction.getDoSupervision());


        companyProduction.setUpdateTime(updatedCompanyProduction.getUpdateTime());
        companyProduction.setUpdateUserId(updatedCompanyProduction.getUpdateUserId());


        CompanyProduction updatedResult = companyProductionRepository.save(companyProduction);
        return updatedResult;
    }



    @DeleteMapping("/company_production/delete/{id}")
    public ResponseEntity<?> deleteCompanyProduction(@PathVariable(value = "id") Long id) {
        CompanyProduction companyProduction = companyProductionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyProduction", "id", id));

        companyProductionRepository.delete(companyProduction);

        return ResponseEntity.ok().build();
    }


    @GetMapping("/company_production/get/{id}")
    public CompanyProduction getCompanyProduction(@PathVariable(value = "id") Long id) {

        CompanyProduction companyProduction = companyProductionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CompanyProduction", "id", id));;

        return companyProduction;
    }
}
