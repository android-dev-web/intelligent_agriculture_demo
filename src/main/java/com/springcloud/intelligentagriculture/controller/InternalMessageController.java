package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.InternalMessageService;
import com.springcloud.intelligentagriculture.Service.ProductionStandardService;
import com.springcloud.intelligentagriculture.Service.WarehouseService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.InternalMessageRepository;
import com.springcloud.intelligentagriculture.repository.ProductionStandardRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "InternalMessage API", produces = "application/json", tags = {"站内消息"})

public class InternalMessageController {

    @Autowired
    InternalMessageRepository internalMessageRepository;
    @Autowired
    InternalMessageService internalMessageService;


    @GetMapping("/internal_message/all")
    @ResponseBody
    public ResponseEntity<String> getAllInternalMessage(@RequestParam(defaultValue = "0") Integer pageNo,
                                                           @RequestParam(defaultValue = "20") Integer pageSize,
                                                           @RequestParam(defaultValue = "id") String sortBy,
                                                           @RequestParam(defaultValue = "0") Long receiveUserId,
                                                           @RequestParam(defaultValue = "0") Long createUserId,
                                                           @RequestParam(defaultValue = "0") Long receiveStatus

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<InternalMessage> list = internalMessageService.getAllInternalMessage(pageNo, pageSize, underscoreSortBy.toString(), receiveUserId, createUserId, receiveStatus);

        List<InternalMessage> totallist = internalMessageRepository.findAllBySearch(receiveUserId, createUserId, receiveStatus);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping("/internal_message/create")
    public InternalMessage createInternalMessage(@Valid @RequestBody InternalMessage internalMessage) {
        if (!internalMessageRepository.existsById(internalMessage.getId()))
            return internalMessageRepository.save(internalMessage);
        else
            return null;
    }

}