package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.*;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.*;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "ProductVariety API", produces = "application/json", tags = {"生产主体/主营产品/品种定义"})

public class ProductVarietyController {

    @Autowired
    ProductVarietyRepository productVarietyRepository;
    @Autowired
    ProductVarietyService productVarietyService;


    @GetMapping("/product_variety/all")
    @ResponseBody
    public ResponseEntity<String> getAllProductVariety(@RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                                    @RequestParam(defaultValue = "id") String sortBy,
                                                    @RequestParam(defaultValue = "0") Long productId


    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<ProductVariety> list = productVarietyService.getAllProductVariety(pageNo, pageSize, underscoreSortBy.toString(), productId);

        List<ProductVariety> totallist = productVarietyRepository.findAllBySearch(productId);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);


        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }



    @GetMapping("/product_variety/get/{id}")
    public ProductVariety getProductVariety(@PathVariable(value = "id") Long id) {

        ProductVariety productVariety = productVarietyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductVariety", "id", id));;

        return productVariety;
    }


    @DeleteMapping("/product_variety/delete/{id}")
    public ResponseEntity<?> deleteProductVariety(@PathVariable(value = "id") Long id) {
        ProductVariety productVariety = productVarietyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductVariety", "id", id));

        productVarietyRepository.delete(productVariety);

        return ResponseEntity.ok().build();
    }


    @PostMapping("/product_variety/create")
    public ProductVariety createProductVariety(@Valid @RequestBody ProductVariety productVariety) {
        if (!productVarietyRepository.existsById(productVariety.getId()))
            return productVarietyRepository.save(productVariety);
        else
            return null;
    }


    @RequestMapping(value="/product_variety/update/{id}", method = RequestMethod.PUT)
    public ProductVariety updateProductVariety(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody ProductVariety updatedProductVariety) {

        ProductVariety productVariety = productVarietyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ProductVariety", "id", id));


        productVariety.setVarietyName(updatedProductVariety.getVarietyName());
        productVariety.setVarietySort(updatedProductVariety.getVarietySort());
        productVariety.setProductId(updatedProductVariety.getProductId());
        productVariety.setDoShare(updatedProductVariety.getDoShare());


        productVariety.setUpdateTime(productVariety.getUpdateTime());
        productVariety.setUpdateUserId(productVariety.getUpdateUserId());

        ProductVariety updateResult = productVarietyRepository.save(productVariety);
        return updateResult;
    }
}