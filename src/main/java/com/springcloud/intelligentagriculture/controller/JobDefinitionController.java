package com.springcloud.intelligentagriculture.controller;


import com.springcloud.intelligentagriculture.Service.JobDefinitionService;
import com.springcloud.intelligentagriculture.Service.SampleCheckService;
import com.springcloud.intelligentagriculture.exception.ResourceNotFoundException;
import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.JobDefinitionRepository;
import com.springcloud.intelligentagriculture.repository.SampleCheckRepository;
import com.springcloud.intelligentagriculture.utils.FileStorageService;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "JobDefinition API", produces = "application/json", tags = {"作业定义"})

public class JobDefinitionController {

    @Autowired
    JobDefinitionRepository jobDefinitionRepository;
    @Autowired
    JobDefinitionService jobDefinitionService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/job_definition/all")
    @ResponseBody
    public ResponseEntity<String> getAllJobDefinition(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                 @RequestParam(defaultValue = "20") Integer pageSize,
                                                                 @RequestParam(defaultValue = "id") String sortBy

    ) {
        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<JobDefinition> list = jobDefinitionService.getAllJobDefinition(pageNo, pageSize, underscoreSortBy.toString());

        JSONObject entity = new JSONObject();
        entity.put("total",jobDefinitionRepository.count());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/job_definition/get/{id}")
    public JobDefinition getJobDefinition(@PathVariable(value = "id") Long id) {

        JobDefinition jobDefinition = jobDefinitionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("JobDefinition", "id", id));;

        return jobDefinition;
    }

    @RequestMapping(value="/job_definition/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public JobDefinition createJobDefinition(MultipartFile file,  JobDefinition jobDefinition ) {


        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            jobDefinition.setJobFile(fileUploadUri);
        }

        if (!jobDefinitionRepository.existsById(jobDefinition.getId()))
            return jobDefinitionRepository.save(jobDefinition);
        else
            return null;
    }

    @RequestMapping(value="/job_definition/update/{id}", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public JobDefinition updateJobDefinition(MultipartFile file, @PathVariable(value = "id") Long id, JobDefinition updatedJobDefinition )
    {

        JobDefinition jobDefinition = jobDefinitionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("JobDefinition", "id", id));

        if( file != null ){
            String fileName = fileStorageService.storeFile(file);
            String fileUploadUri = "/uploads/" + fileName.toString();
            jobDefinition.setJobFile(fileUploadUri);
        }

        jobDefinition.setJobName(updatedJobDefinition.getJobName());
        jobDefinition.setJobType(updatedJobDefinition.getJobType());
        jobDefinition.setUpdateTime(updatedJobDefinition.getUpdateTime());
        jobDefinition.setUpdateUserId(updatedJobDefinition.getUpdateUserId());

        JobDefinition updatedResult = jobDefinitionRepository.save(jobDefinition);
        return updatedResult;
    }

    @DeleteMapping("/job_definition/delete/{id}")
    public ResponseEntity<?> deleteJobDefinition(@PathVariable(value = "id") Long id) {
        JobDefinition jobDefinition = jobDefinitionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("JobDefinition", "id", id));

        jobDefinitionRepository.delete(jobDefinition);

        return ResponseEntity.ok().build();
    }

}