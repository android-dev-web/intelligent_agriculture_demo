package com.springcloud.intelligentagriculture.controller;

import com.springcloud.intelligentagriculture.Service.PublicLicenseService;
import com.springcloud.intelligentagriculture.model.PublicLicense;
import com.springcloud.intelligentagriculture.repository.PublicLicenseRepository;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")

@Api(description = "PublicLicense API", produces = "application/json", tags = {"行政许可信息"})

public class PublicLicenseController {

    @Autowired
    PublicLicenseRepository publicLicenseRepository;
    @Autowired
    PublicLicenseService publicLicenseService;


    @GetMapping("/public_license/all")
    @ResponseBody
    public ResponseEntity<String> getAllQualityStandards(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                      @RequestParam(defaultValue = "20") Integer pageSize,
                                                                      @RequestParam(defaultValue = "id") String sortBy,
                                                                      @RequestParam(defaultValue = "") String creditCode
                                                                      ) {

        Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(sortBy);
        StringBuffer underscoreSortBy = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(underscoreSortBy, "_"+m.group().toLowerCase());
        }
        m.appendTail(underscoreSortBy);

        List<PublicLicense> list = publicLicenseService.getAllPublicLicenses(pageNo, pageSize, underscoreSortBy.toString(), creditCode);

        List<PublicLicense> totallist = publicLicenseRepository.findAllBySearch(creditCode);

        JSONObject entity = new JSONObject();
        entity.put("total",totallist.size());
        entity.put("data",list);

        return new ResponseEntity<String>(entity.toString(), new HttpHeaders(), HttpStatus.OK);
    }
}