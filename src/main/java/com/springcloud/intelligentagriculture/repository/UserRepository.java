package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface UserRepository extends JpaRepository<Users, Long>, PagingAndSortingRepository<Users, Long> {

    @Query(value = "SELECT * FROM user WHERE user_id=:name", nativeQuery = true)
    Users findByUsername(@Param("name") String name);
}
