package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VillageRepository extends JpaRepository<Village, Long>{

    @Query(value = "SELECT t.* FROM village AS t WHERE ( 0 = :province_id or t.province_id = :province_id ) and ( 0 = :city_id or t.city_id = :city_id ) and ( 0 = :country_id or t.country_id = :country_id )",
            countQuery = "SELECT count(*) ROM village AS t WHERE ( 0 = :province_id or t.province_id = :province_id ) and ( 0 = :city_id or t.city_id = :city_id ) and ( 0 = :country_id or t.country_id = :country_id )",
            nativeQuery = true)
    public List<Village> findAllBySearch(@Param("province_id") Long province_id, @Param("city_id") Long city_id, @Param("country_id") Long country_id  );

}
