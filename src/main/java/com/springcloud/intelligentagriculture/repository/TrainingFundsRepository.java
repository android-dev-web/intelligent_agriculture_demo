package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.TrainingFunds;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

@Repository
public interface TrainingFundsRepository extends JpaRepository<TrainingFunds, Long>, PagingAndSortingRepository<TrainingFunds, Long> {

    @Query(value = "SELECT t.* FROM training_funds AS t " +
            "WHERE " +
            "(-1 = :special_flag or t.special_flag = :special_flag) and " +
            "('' = :create_time or t.create_time = :create_time) and " +
            "('' = :create_time_from or t.create_time >= :create_time_from) and " +
            "('' = :create_time_to or create_time <= :create_time_to)",

            countQuery = "SELECT count(*) FROM training_funds AS t " +
                    "WHERE " +
                    "(-1 = :special_flag or t.special_flag = :special_flag) and " +
                    "('' = :create_time or t.create_time = :create_time) and " +
                    "('' = :create_time_from or t.create_time >= :create_time_from) and " +
                    "('' = :create_time_to or create_time <= :create_time_to)",

            nativeQuery = true)
    public Page<TrainingFunds> findAllBySearch(Pageable pageable, @Param("special_flag") Integer special_flag, @Param("create_time") String create_time, @Param("create_time_from") String create_time_from, @Param("create_time_to") String create_time_to);

    @Query(value = "SELECT t.* FROM training_funds AS t " +
            "WHERE " +
            "(-1 = :special_flag or t.special_flag = :special_flag) and " +
            "('' = :create_time or t.create_time = :create_time) and " +
            "('' = :create_time_from or t.create_time >= :create_time_from) and " +
            "('' = :create_time_to or create_time <= :create_time_to)",

            countQuery = "SELECT count(*) FROM training_funds AS t " +
                    "WHERE " +
                    "(-1 = :special_flag or t.special_flag = :special_flag) and " +
                    "('' = :create_time or t.create_time = :create_time) and " +
                    "('' = :create_time_from or t.create_time >= :create_time_from) and " +
                    "('' = :create_time_to or create_time <= :create_time_to)",

            nativeQuery = true)
    public List<TrainingFunds> findAllBySearch(@Param("special_flag") Integer special_flag, @Param("create_time") String create_time, @Param("create_time_from") String create_time_from, @Param("create_time_to") String create_time_to);

//    @Query(value = "SELECT t.* FROM training_funds AS t WHERE (-1 = :special_flag or t.special_flag = :special_flag)",
//            countQuery = "SELECT count(*) FROM training_funds AS t WHERE (-1 = :special_flag or t.special_flag = :special_flag)",
//            nativeQuery = true)
//    public Page<TrainingFunds> findAllBySearch(Pageable pageable, @Param("special_flag") Integer special_flag);
//
//    @Query(value = "SELECT t.* FROM training_funds AS t WHERE (-1 = :special_flag or t.special_flag = :special_flag)",
//            countQuery = "SELECT count(*) FROM training_funds AS t WHERE (-1 = :special_flag or t.special_flag = :special_flag)",
//            nativeQuery = true)
//    public List<TrainingFunds> findAllBySearch(@Param("special_flag") Integer special_flag);
}
