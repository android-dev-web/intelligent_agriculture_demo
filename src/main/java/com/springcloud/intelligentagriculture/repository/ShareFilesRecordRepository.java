package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.CompanyProduction;
import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.ShareFilesRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface ShareFilesRecordRepository extends JpaRepository<ShareFilesRecord, Long>, PagingAndSortingRepository<ShareFilesRecord, Long> {

    @Query(value = "SELECT t.* FROM share_files_record AS t WHERE  (-1 = :share_files_id or t.share_files_id = :share_files_id) and ('' = :upload_company or t.upload_company = :upload_company)",
            countQuery = "SELECT count(*) FROM share_files_record AS t WHERE  (-1 = :share_files_id or t.share_files_id = :share_files_id) and ('' = :upload_company or t.upload_company = :upload_company)", nativeQuery = true)
    public Page<ShareFilesRecord> findAllBySearch(Pageable pageable, @Param("share_files_id") int share_files_id, @Param("upload_company") String upload_company );

    @Query(value = "SELECT t.* FROM share_files_record AS t WHERE  (-1 = :share_files_id or t.share_files_id = :share_files_id) and ('' = :upload_company or t.upload_company = :upload_company)",
            countQuery = "SELECT count(*) FROM share_files_record AS t WHERE  (-1 = :share_files_id or t.share_files_id = :share_files_id) and ('' = :upload_company or t.upload_company = :upload_company)", nativeQuery = true)
    public List<ShareFilesRecord> findAllBySearch(@Param("share_files_id") int share_files_id, @Param("upload_company") String upload_company );
}
