package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.CompanyProduction;
import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.ShareFiles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface ShareFilesRepository extends JpaRepository<ShareFiles, Long>, PagingAndSortingRepository<ShareFiles, Long> {

    @Query(value = "SELECT t.* FROM share_files AS t WHERE -1 = :is_visible or t.is_visible = :is_visible",
            countQuery = "SELECT count(*) FROM share_files AS t WHERE -1 = :is_visible or t.is_visible = :is_visible", nativeQuery = true)
    public Page<ShareFiles> findAllBySearch(Pageable pageable, @Param("is_visible") int is_visible);


    @Query(value = "SELECT t.* FROM share_files AS t WHERE -1 = :is_visible or t.is_visible = :is_visible",
            countQuery = "SELECT count(*) FROM share_files AS t WHERE -1 = :is_visible or t.is_visible = :is_visible", nativeQuery = true)
    public List<ShareFiles> findAllBySearch( @Param("is_visible") int is_visible);
}
