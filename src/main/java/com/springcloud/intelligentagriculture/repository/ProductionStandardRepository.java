package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionStandardRepository extends JpaRepository<ProductionStandard, Long>, PagingAndSortingRepository<ProductionStandard, Long> {

    @Query(value = "SELECT t.* FROM production_standard AS t WHERE ( '' = :product_name or t.product_name = :product_name ) and ('0' = :category or t.category = :category)",
            countQuery = "SELECT count(*) FROM production_standard AS t WHERE ( '' = :product_name or t.product_name = :product_name ) and ('0' = :category or t.category = :category)",
            nativeQuery = true)
    public Page<ProductionStandard> findAllBySearch(Pageable pageable, @Param("product_name") String product_name, @Param("category") String category );

    @Query(value = "SELECT t.* FROM production_standard AS t WHERE ( '' = :product_name or t.product_name = :product_name ) and ('0' = :category or t.category = :category)",
            countQuery = "SELECT count(*) FROM production_standard AS t WHERE ( '' = :product_name or t.product_name = :product_name ) and ('0' = :category or t.category = :category)",
            nativeQuery = true)
    public List<ProductionStandard> findAllBySearch(@Param("product_name") String product_name, @Param("category") String category );

}

