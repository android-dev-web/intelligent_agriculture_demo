package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface WorkTaskRepository extends JpaRepository<WorkTask, Long>, PagingAndSortingRepository<WorkTask, Long> {

      @Query(value = "SELECT t.* FROM work_task AS t WHERE ('' = :release_time_from or t.release_time >= :release_time_from ) and ('' = :release_time_to or t.release_time <= :release_time_to)",
              countQuery = "SELECT count(*) FROM work_task AS t WHERE ('' = :release_time_from or t.release_time >= :release_time_from ) and ('' = :release_time_to or t.release_time <= :release_time_to)",
              nativeQuery = true)
      public Page<WorkTask> findAllBySearch(Pageable pageable, @Param("release_time_from") String release_time_from, @Param("release_time_to") String release_time_to );

      @Query(value = "SELECT t.* FROM work_task AS t WHERE ('' = :release_time_from or t.release_time >= :release_time_from ) and ('' = :release_time_to or t.release_time <= :release_time_to)",
              countQuery = "SELECT count(*) FROM work_task AS t WHERE ('' = :release_time_from or t.release_time >= :release_time_from ) and ('' = :release_time_to or t.release_time <= :release_time_to)",
              nativeQuery = true)
      public List<WorkTask> findAllBySearch(@Param("release_time_from") String release_time_from, @Param("release_time_to") String release_time_to );

}

