package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeRepository extends JpaRepository<Notice, Long>, PagingAndSortingRepository<Notice, Long> {

    @Query(value = "SELECT t.* FROM notice AS t WHERE '0' = :emergency_degree or t.emergency_degree = :emergency_degree",
            countQuery = "SELECT count(*) FROM notice AS t WHERE '0' = :emergency_degree or t.emergency_degree = :emergency_degree",
            nativeQuery = true)
    public Page<Notice> findAllBySearch(Pageable pageable, @Param("emergency_degree") String emergency_degree);

    @Query(value = "SELECT t.* FROM notice AS t WHERE '0' = :emergency_degree or t.emergency_degree = :emergency_degree",
            countQuery = "SELECT count(*) FROM notice AS t WHERE '0' = :emergency_degree or t.emergency_degree = :emergency_degree",
            nativeQuery = true)
    public List<Notice> findAllBySearch(@Param("emergency_degree") String emergency_degree);

}

