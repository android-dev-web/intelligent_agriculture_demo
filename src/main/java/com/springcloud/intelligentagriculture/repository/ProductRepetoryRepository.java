package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductRepetory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepetoryRepository extends JpaRepository<ProductRepetory, Long>, PagingAndSortingRepository<ProductRepetory, Long> {

    @Query(value = "SELECT t.* FROM product_repetory AS t WHERE 0 = :product_id or t.product_id = :product_id",
            countQuery = "SELECT count(*) FROM product_repetory AS t WHERE 0 = :product_id or t.product_id = :product_id",
            nativeQuery = true)
    public Page<ProductRepetory> findAllBySearch(Pageable pageable, @Param("product_id") int product_id );

    @Query(value = "SELECT t.* FROM product_repetory AS t WHERE 0 = :product_id or t.product_id = :product_id",
            countQuery = "SELECT count(*) FROM product_repetory AS t WHERE 0 = :product_id or t.product_id = :product_id",
            nativeQuery = true)
    public List<ProductRepetory> findAllBySearch(@Param("product_id") int product_id );

}

