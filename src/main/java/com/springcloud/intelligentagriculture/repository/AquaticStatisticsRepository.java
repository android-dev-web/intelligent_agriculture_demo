package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.CompanyProduction;
import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.AquaticStatistics;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface AquaticStatisticsRepository extends JpaRepository<AquaticStatistics, Long>, PagingAndSortingRepository<AquaticStatistics, Long> {

}
