package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InternalMessageRepository extends JpaRepository<InternalMessage, Long>, PagingAndSortingRepository<InternalMessage, Long> {

    @Query(value = "SELECT t.* FROM internal_message AS t WHERE ( 0 = :receive_user_id or t.receive_user_id = :receive_user_id ) and (0 = :create_user_id or t.create_user_id = :create_user_id) and (0 = :receive_status or t.receive_status = :receive_status)",
            countQuery = "SELECT count(*) FROM internal_message AS t WHERE ( 0 = :receive_user_id or t.receive_user_id = :receive_user_id ) and (0 = :create_user_id or t.create_user_id = :create_user_id) and (0 = :receive_status or t.receive_status = :receive_status)",
            nativeQuery = true)
    public Page<InternalMessage> findAllBySearch(Pageable pageable, @Param("receive_user_id") Long receive_user_id, @Param("create_user_id") Long create_user_id, @Param("receive_status") Long receive_status  );

    @Query(value = "SELECT t.* FROM internal_message AS t WHERE ( 0 = :receive_user_id or t.receive_user_id = :receive_user_id ) and (0 = :create_user_id or t.create_user_id = :create_user_id) and (0 = :receive_status or t.receive_status = :receive_status)",
            countQuery = "SELECT count(*) FROM internal_message AS t WHERE ( 0 = :receive_user_id or t.receive_user_id = :receive_user_id ) and (0 = :create_user_id or t.create_user_id = :create_user_id) and (0 = :receive_status or t.receive_status = :receive_status)",
            nativeQuery = true)
    public List<InternalMessage> findAllBySearch(@Param("receive_user_id") Long receive_user_id, @Param("create_user_id") Long create_user_id, @Param("receive_status") Long receive_status  );
}

