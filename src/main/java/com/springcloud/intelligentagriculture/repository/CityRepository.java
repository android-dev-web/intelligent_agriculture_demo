package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.City;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import com.springcloud.intelligentagriculture.model.Province;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{

    @Query(value = "SELECT t.* FROM city AS t WHERE 0 = :province_id or t.province_id = :province_id",
            countQuery = "SELECT count(*) FROM city AS t WHERE 0 = :province_id or t.province_id = :province_id",
            nativeQuery = true)
    public List<City> findAllBySearch(@Param("province_id") Long province_id );

}
