package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.Town;
import com.springcloud.intelligentagriculture.model.InputsUse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface InputsUseRepository extends JpaRepository<InputsUse, Long>, PagingAndSortingRepository<InputsUse, Long> {

}
