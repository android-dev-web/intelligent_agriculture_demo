package com.springcloud.intelligentagriculture.repository;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.model.ProductBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface WorkTaskReportRepository extends JpaRepository<WorkTaskReport, Long>, PagingAndSortingRepository<WorkTaskReport, Long> {

    @Query(value = "SELECT t.* FROM work_task_report AS t WHERE 0 = :work_task_id or t.work_task_id = :work_task_id",
            countQuery = "SELECT count(*) FROM work_task_report AS t WHERE 0 = :work_task_id or t.work_task_id = :work_task_id",
            nativeQuery = true)
    public Page<WorkTaskReport> findAllBySearch(Pageable pageable, @Param("work_task_id") Long work_task_id );

    @Query(value = "SELECT t.* FROM work_task_report AS t WHERE 0 = :work_task_id or t.work_task_id = :work_task_id",
            countQuery = "SELECT count(*) FROM work_task_report AS t WHERE 0 = :work_task_id or t.work_task_id = :work_task_id",
            nativeQuery = true)
    public List<WorkTaskReport> findAllBySearch(@Param("work_task_id") Long work_task_id );
}

