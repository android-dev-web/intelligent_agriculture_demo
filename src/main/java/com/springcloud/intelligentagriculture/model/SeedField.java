package com.springcloud.intelligentagriculture.model;

import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class SeedField{

    @ApiModelProperty(value = "户名")
    private String field1;
    @ApiModelProperty(value = "品种名称")
    private String field2;
    @ApiModelProperty(value = "补贴面积(亩)")
    private String field3;
    @ApiModelProperty(value = "供种面积(亩)")
    private String field4;
    @ApiModelProperty(value = "中标价格（元/公斤）")
    private String field5;
    @ApiModelProperty(value = "种子应收金额（元）")
    private String field6;
    @ApiModelProperty(value = "种子补贴金额（元）")
    private String field7;
    @ApiModelProperty(value = "农户自负金额（元）")
    private String field8;

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) { this.field1 = field1; }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) { this.field2 = field2; }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) { this.field3 = field3; }

    public String getField4() { return field4; }

    public void setField4(String field4) { this.field4 = field4; }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) { this.field5 = field5; }

    public String getField6() {
        return field6;
    }

    public void setField6(String field6) { this.field6 = field6; }

    public String getField7() {
        return field7;
    }

    public void setField7(String field7) { this.field7 = field7; }

    public String getField8() {
        return field8;
    }

    public void setField8(String field8) { this.field8 = field8; }
}



