package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "product_batch")
@EntityListeners(AuditingEntityListener.class)


public class ProductBatch implements Serializable {

    @ApiModelProperty(value = "产品批次ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "'品种")
    @Length(max = 11, message = "*variety must have at max 11 characters")
    private String variety;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyOption() {
        return propertyOption;
    }

    public void setPropertyOption(String propertyOption) {
        this.propertyOption = propertyOption;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "'等级名称")
    @Length(max = 11, message = "*grade must have at max 11 characters")
    private String grade;

    @ApiModelProperty(value = "'产品ID")
    @NotNull
    private Long productId;

    @ApiModelProperty(value = "'属性ID")
    @NotNull
    private Long propertyId;

    @ApiModelProperty(value = "'属性名称")
    @NotNull
    @Length(max = 50, message = "*propertyName must have at max 50 characters")
    private String propertyName;

    @ApiModelProperty(value = "'属性项")
    @NotNull
    @Length(max = 50, message = "*propertyOption must have at max 50 characters")
    private String propertyOption;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;


}