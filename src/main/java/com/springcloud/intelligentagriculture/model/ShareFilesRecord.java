package com.springcloud.intelligentagriculture.model;

import net.bytebuddy.implementation.bind.annotation.Default;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "ShareFilesRecord")
@EntityListeners(AuditingEntityListener.class)

public class ShareFilesRecord implements Serializable {

    @ApiModelProperty(value = "共享文件 record ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "共享文件 ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int shareFilesId;

    @ApiModelProperty(value = "上传单位")
    @Length(max = 128, message = "*uploadCompany must have at max 128 characters")
    private String uploadCompany;

    @ApiModelProperty(value = "上传者")
    @Length(max = 128, message = "*uploadPerson must have at max 128 characters")
    private String uploadPerson;

    @ApiModelProperty(value = "上传文件")
    @Length(max = 128, message = "*uploadProfile must have at max 128 characters")
    private String uploadProfile;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public int getShareFilesId() {
        return shareFilesId;
    }

    public void setShareFilesId(int shareFilesId) {
        this.shareFilesId = shareFilesId;
    }

    public String getUploadCompany() {
        return uploadCompany;
    }

    public void setUploadCompany(String uploadCompany) {
        this.uploadCompany = uploadCompany;
    }

    public String getUploadPerson() {
        return uploadPerson;
    }

    public void setUploadPerson(String uploadPerson) {
        this.uploadPerson = uploadPerson;
    }

    public String getUploadProfile() {
        return uploadProfile;
    }

    public void setUploadProfile(String uploadProfile) {
        this.uploadProfile = uploadProfile;
    }

    public Date getCreateTime() { return createTime;}

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}


