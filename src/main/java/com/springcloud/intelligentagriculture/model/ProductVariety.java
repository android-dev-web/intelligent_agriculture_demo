package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "product_variety")
@EntityListeners(AuditingEntityListener.class)


public class ProductVariety implements Serializable {

    @ApiModelProperty(value = "品种ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarietyName() {
        return varietyName;
    }

    public void setVarietyName(String varietyName) {
        this.varietyName = varietyName;
    }

    public String getVarietySort() {
        return varietySort;
    }

    public void setVarietySort(String varietySort) {
        this.varietySort = varietySort;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDoShare() {
        return doShare;
    }

    public void setDoShare(String doShare) {
        this.doShare = doShare;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "品种名称")
    @Length(max = 255, message = "*varietyName must have at max 255 characters")
    private String varietyName;


    @ApiModelProperty(value = "品种排序")
    @Length(max = 255, message = "*varietySort must have at max 255 characters")
    private String varietySort;


    @ApiModelProperty(value = "产品ID")
    private Long productId;


    @ApiModelProperty(value = "是否共享(0:否，1：是)")
    @Length(max = 11, message = "*doShare must have at max 11 characters")
    private String doShare;


    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;


}