package com.springcloud.intelligentagriculture.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.print.DocFlavor;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "training_funds")
@EntityListeners(AuditingEntityListener.class)

public class TrainingFunds implements Serializable {

    @ApiModelProperty(value = "培训经费ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "项目名称")
    @Length(max = 255, message = "*projectName must have at max 255 characters")
    private String projectName;

    @ApiModelProperty(value = "申请金额")
    private Double appliedAmount;

    @ApiModelProperty(value = "申请人")
    @Length(max = 255, message = "*proposer must have at max 255 characters")
    private String proposer;

    @ApiModelProperty(value = "所在单位ID")
    private Long companyId;

    @ApiModelProperty(value = "培训经费附件")
    @Length(max = 1000, message = "*traningFundsProfiles must have at max 1000 characters")
    private String trainingFundsProfiles;

    @ApiModelProperty(value = "创建时间(YYYY-MM-DD HH:MM:SS)")
    private String createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间(YYYY-MM-DD HH:MM:SS)")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    @ApiModelProperty(value = "专项")
    private int specialFlag;

    public int getSpecialFlag() { return specialFlag; }

    public void setSpecialFlag(int specialFlag) { this.specialFlag = specialFlag; }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Double getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(Double appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getProposer() {
        return proposer;
    }

    public void setProposer(String proposer) {
        this.proposer = proposer;
    }

    public Long getCompanyId() { return companyId; }

    public void setCompanyId(Long companyId) { this.companyId = companyId; }

    public String getTrainingFundsProfiles() {
        return trainingFundsProfiles;
    }

    public void setTrainingFundsProfiles(String trainingFundsProfiles) {
        this.trainingFundsProfiles = trainingFundsProfiles;
    }

    public String getCreateTime() { return createTime;}

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

}


