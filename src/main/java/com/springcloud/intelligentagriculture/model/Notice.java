package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "notice")
@EntityListeners(AuditingEntityListener.class)


public class Notice implements Serializable {

    @ApiModelProperty(value = "公告ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "发布日期")
    private Date releaseTime;

    @ApiModelProperty(value = "发布人")
    @Length(max = 50, message = "*releasePerson must have at max 50 characters")
    private String releasePerson;

    @ApiModelProperty(value = "标题")
    @Length(max = 255, message = "*title must have at max 255 characters")
    private String title;

    @ApiModelProperty(value = "内容")
    @Length(max = 500, message = "*content must have at max 500 characters")
    private String content;

    @ApiModelProperty(value = "公告附件")
    @Length(max = 1000, message = "*noticeProfiles must have at max 1000 characters")
    private String noticeProfiles;

    @ApiModelProperty(value = "紧急程度(1:低,2:中,3:高)")
    @Length(max = 255, message = "*emergencyDegree must have at max 255 characters")
    private String emergencyDegree;

    @ApiModelProperty(value = "类型")
    @Length(max = 255, message = "*type must have at max 255 characters")
    private String type;

    @ApiModelProperty(value = "定时发布")
    @Length(max = 255, message = "*timingRelease must have at max 255 characters")
    private String timingRelease;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getReleasePerson() {
        return releasePerson;
    }

    public void setReleasePerson(String releasePerson) {
        this.releasePerson = releasePerson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNoticeProfiles() {
        return noticeProfiles;
    }

    public void setNoticeProfiles(String noticeProfiles) {
        this.noticeProfiles = noticeProfiles;
    }

    public String getEmergencyDegree() {
        return emergencyDegree;
    }

    public void setEmergencyDegree(String emergencyDegree) {
        this.emergencyDegree = emergencyDegree;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimingRelease() {
        return timingRelease;
    }

    public void setTimingRelease(String timingRelease) {
        this.timingRelease = timingRelease;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

}