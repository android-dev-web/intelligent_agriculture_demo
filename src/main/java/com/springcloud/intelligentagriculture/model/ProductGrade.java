package com.springcloud.intelligentagriculture.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "product_grade")
@EntityListeners(AuditingEntityListener.class)


public class ProductGrade implements Serializable {

    @ApiModelProperty(value = "等级ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "等级名称")
    @Length(max = 255, message = "*gradeName must have at max 255 characters")
    private String gradeName;

    @ApiModelProperty(value = "等级排序")
    @Length(max = 255, message = "*gradeSort must have at max 255 characters")
    private String gradeSort;

    @ApiModelProperty(value = "产品ID : product_production")
    private Long productId;

    @ApiModelProperty(value = "是否共享(0:否，1：是)")
    @NotNull
    @Length(max = 255, message = "*doShare must have at max 255 characters")
    private String doShare;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getGradeName() { return gradeName; }

    public void setGradeName(String gradeName) { this.gradeName = gradeName; }

    public String getGradeSort() { return gradeSort; }

    public void setGradeSort(String gradeSort) { this.gradeSort = gradeSort; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public String getDoShare() { return doShare; }

    public void setDoShare(String doShare) { this.doShare = doShare; }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}
