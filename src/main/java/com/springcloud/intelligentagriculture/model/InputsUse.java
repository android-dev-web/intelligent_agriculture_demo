package com.springcloud.intelligentagriculture.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "InputsUse")
@EntityListeners(AuditingEntityListener.class)

public class InputsUse implements Serializable {

    @ApiModelProperty(value = "投入品ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "生产主体ID")
    private Long companyId;

    @ApiModelProperty(value = "样品名称")
    @Length(max = 255, message = "*sampleName must have at max 255 characters")
    private String sampleName;

    @ApiModelProperty(value = "使用数量")
    private Double amount;

    @ApiModelProperty(value = "投入品附件")
    @Length(max = 1000, message = "*inputProfiles must have at max 1000 characters")
    private String inputProfiles;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getCompanyId() { return companyId; }

    public void setCompanyId(Long companyId) { this.companyId = companyId; }

    public String getSampleName() { return sampleName; }

    public void setSampleName(String sampleName) { this.sampleName = sampleName; }

    public Double getAmount() { return amount; }

    public void setAmount(Double amount) { this.amount = amount; }

    public String getInputProfiles() { return inputProfiles; }

    public void setInputProfiles(String inputProfiles) { this.inputProfiles = inputProfiles; }

    public Date getCreateTime() { return createTime;}

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}


