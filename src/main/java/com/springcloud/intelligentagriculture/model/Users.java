package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "user ")
@EntityListeners(AuditingEntityListener.class)


public class Users implements Serializable {

    @ApiModelProperty(value = "常用语ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty(value = "登录")
    @Length(max = 128, message = "*userId must have at max 128 characters")
    private String userId;

    @ApiModelProperty(value = "名称")
    @NotNull
    @Length(max = 32, message = "*contactName must have at max 32 characters")
    private String contactName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Long getUserType() {
        return userType;
    }

    public void setUserType(Long userType) {
        this.userType = userType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactWay() {
        return contactWay;
    }

    public void setContactWay(String contactWay) {
        this.contactWay = contactWay;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "角色 (1:市级管理员, 2:乡镇管理员, 3: 普通用户)")
    @NotNull
    private Long userType;

    @ApiModelProperty(value = "密码")
    @NotNull
    @Length(max = 256, message = "*password must have at max 32 characters")
    private String password;

    @ApiModelProperty(value = "联系人")
    @NotNull
    @Length(max = 32, message = "*contactPerson must have at max 32 characters")
    private String contactPerson;

    @ApiModelProperty(value = "联系方式")
    @NotNull
    @Length(max = 32, message = "*contactWay must have at max 32 characters")
    private String contactWay;

    @ApiModelProperty(value = "'统一社会信用代码")
    @NotNull
    @Length(max = 64, message = "*creditCode must have at max 64 characters")
    private String creditCode;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    @ApiModelProperty(value = "登录")
    @Length(max = 2048, message = "*token must have at max 2048 characters")
    private String token;

}


