package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "production_standard")
@EntityListeners(AuditingEntityListener.class)


public class ProductionStandard implements Serializable {

    @ApiModelProperty(value = "生产标准ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "产品名称")
    @Length(max = 50, message = "*workTaskProfiles must have at max 50 characters")
    private String productName;

    @ApiModelProperty(value = "类别(1:畜牧业,2:水产业)")
    @Length(max = 11, message = "*workTaskProfiles must have at max 11 characters")
    private String category;

    @ApiModelProperty(value = "发布日期")
    private Date releaseTime;

    @ApiModelProperty(value = "发布人")
    @Length(max = 50, message = "*workTaskProfiles must have at max 50 characters")
    private String releasePerson;

    @ApiModelProperty(value = "生产标准")
    @Length(max = 1000, message = "*productionStandard must have at max 1000 characters")
    private String productStandard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getReleasePerson() {
        return releasePerson;
    }

    public void setReleasePerson(String releasePerson) {
        this.releasePerson = releasePerson;
    }

    public String getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(String productStandard) {
        this.productStandard = productStandard;
    }

    public String getProductionStandardProfiles() {
        return productionStandardProfiles;
    }

    public void setProductionStandardProfiles(String productionStandardProfiles) {
        this.productionStandardProfiles = productionStandardProfiles;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "生产标准附件")
    @Length(max = 1000, message = "*productionStandardProfiles must have at max 1000 characters")
    private String productionStandardProfiles;


    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;
}