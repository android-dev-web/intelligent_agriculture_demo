package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "product_task")
@EntityListeners(AuditingEntityListener.class)


public class ProductTask implements Serializable {

    @ApiModelProperty(value = "作业ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty(value = "作业名称")
    @Length(max = 50, message = "*taskName must have at max 50 characters")
    private String taskName;


    @ApiModelProperty(value = "作业类型(1:收获前,2:收获,3:收获后)")
    @Length(max = 11, message = "*taskType must have at max 11 characters")
    private String taskType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskImages() {
        return taskImages;
    }

    public void setTaskImages(String taskImages) {
        this.taskImages = taskImages;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDoShare() {
        return doShare;
    }

    public void setDoShare(String doShare) {
        this.doShare = doShare;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "作业图片")
    @Length(max = 1000, message = "*taskImages must have at max 1000 characters")
    private String taskImages;


    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "是否共享(0:否，1：是)")
    @NotNull
    @Length(max = 11, message = "*doShare must have at max 11 characters")
    private String doShare;


    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;


}