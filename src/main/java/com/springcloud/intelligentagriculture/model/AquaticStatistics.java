package com.springcloud.intelligentagriculture.model;

import net.bytebuddy.implementation.bind.annotation.Default;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "aquatic_statistics")
@EntityListeners(AuditingEntityListener.class)

public class AquaticStatistics implements Serializable {

    @ApiModelProperty(value = "水产统计ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "序号")
    @Length(max = 20, message = "*serialNum must have at max 20 characters")
    private String serialNum;

    @ApiModelProperty(value = "样品名称")
    @Length(max = 255, message = "*sampleName must have at max 255 characters")
    private String sampleName;

    @ApiModelProperty(value = "样品学名")
    @Length(max = 255, message = "*sampleScientificName must have at max 255 characters")
    private String sampleScientificName;

    @ApiModelProperty(value = "样品编号")
    @Length(max = 20, message = "*sampleSerialNum must have at max 20 characters")
    private String sampleSerialNum;

    @ApiModelProperty(value = "市别")
    @Length(max = 255, message = "*city must have at max 255 characters")
    private String city;

    @ApiModelProperty(value = "区县")
    @Length(max = 255, message = "*district must have at max 255 characters")
    private String district;

    @ApiModelProperty(value = "被检单位")
    @Length(max = 255, message = "*inspectedUnit must have at max 255 characters")
    private String inspectedUnit;

    @ApiModelProperty(value = "样品类型")
    @Length(max = 255, message = "*sampleType must have at max 255 characters")
    private String sampleType;

    @ApiModelProperty(value = "呋喃唑酮代谢物")
    @Length(max = 255, message = "*furaMeta must have at max 255 characters")
    private String furaMeta;

    @ApiModelProperty(value = "孔雀石绿")
    @Length(max = 255, message = "*malaGreen must have at max 255 characters")
    private String malaGreen;

    @ApiModelProperty(value = "检测室编号")
    @Length(max = 20, message = "*detectionRoomNum must have at max 20 characters")
    private String detectionRoomNum;

    @ApiModelProperty(value = "检测时间")
    @Length(max = 255, message = "*detectionRoomName must have at max 255 characters")
    private String detectionRoomName;

    @ApiModelProperty(value = "检测时间")
    private Date detectionTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getSerialNum() { return serialNum; }

    public void setSerialNum(String serialNum) { this.serialNum = serialNum; }

    public String getSampleName() { return sampleName; }

    public void setSampleName(String sampleName) { this.sampleName = sampleName; }

    public String getSampleScientificName() { return sampleScientificName; }

    public void setSampleScientificName(String sampleScientificName) { this.sampleScientificName = sampleScientificName; }

    public String getSampleSerialNum() { return sampleSerialNum; }

    public void setSampleSerialNum(String sampleSerialNum) { this.sampleSerialNum = sampleSerialNum; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getDistrict() { return district; }

    public void setDistrict(String district) { this.district = district; }

    public String getInspectedUnit() { return inspectedUnit; }

    public void setInspectedUnit(String inspectedUnit) { this.inspectedUnit = inspectedUnit; }

    public String getSampleType() { return sampleType; }

    public void setSampleType(String sampleType) { this.sampleType = sampleType; }

    public String getFuraMeta() { return furaMeta; }

    public void setFuraMeta(String furaMeta) { this.furaMeta = furaMeta; }

    public String getMalaGreen() { return malaGreen; }

    public void setMalaGreen(String malaGreen) { this.malaGreen = malaGreen; }

    public String getDetectionRoomNum() { return detectionRoomNum; }

    public void setDetectionRoomNum(String detectionRoomNum) { this.detectionRoomNum = detectionRoomNum; }

    public String getDetectionRoomName() { return detectionRoomName; }

    public void setDetectionRoomName(String detectionRoomName) { this.detectionRoomName = detectionRoomName; }

    public Date getDetectionTime() { return detectionTime; }

    public void setDetectionTime(Date detectionTime) { this.detectionTime = detectionTime; }

    public Date getCreateTime() { return createTime;}

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}


