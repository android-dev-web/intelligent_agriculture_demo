package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "production_record")
@EntityListeners(AuditingEntityListener.class)


public class ProductionRecord implements Serializable {

    @ApiModelProperty(value = "生产记录ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty(value = "产品ID")
    @NotNull
    private Long productId;


    @ApiModelProperty(value = "产品名称")
    @Length(max = 50, message = "*productName must have at max 128 characters")
    private String productName;

    @ApiModelProperty(value = "产量")
    private Float productionQuantity;

    @ApiModelProperty(value = "产地")
    @Length(max = 255, message = "*productionArea must have at max 255 characters")
    private String productionArea;

    @ApiModelProperty(value = "生产时长")
    private Float productionTime;

    @ApiModelProperty(value = "生产记录附件")
    @Length(max = 500, message = "*productionProfiles must have at max 128 characters")
    private String productionProfiles;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getProductionQuantity() {
        return productionQuantity;
    }

    public void setProductionQuantity(Float productionQuantity) {
        this.productionQuantity = productionQuantity;
    }

    public String getProductionArea() {
        return productionArea;
    }

    public void setProductionArea(String productionArea) {
        this.productionArea = productionArea;
    }

    public Float getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(Float productionTime) {
        this.productionTime = productionTime;
    }

    public String getProductionProfiles() {
        return productionProfiles;
    }

    public void setProductionProfiles(String productionProfiles) {
        this.productionProfiles = productionProfiles;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;
}