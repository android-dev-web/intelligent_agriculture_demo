package com.springcloud.intelligentagriculture.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "Seed")
@EntityListeners(AuditingEntityListener.class)

public class Seed implements Serializable {

    @ApiModelProperty(value = "种子ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "种子附件")
    @Length(max = 1000, message = "*seedProfiles must have at max 1000 characters")
    private String seedProfiles;

    @ApiModelProperty(value = "村ID")
    private Long villageId;

    @ApiModelProperty(value = "乡镇ID")
    private Long townId;

    @ApiModelProperty(value = "市ID")
    private Long cityId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getSeedProfiles() {
        return seedProfiles;
    }

    public void setSeedProfiles(String seedProfiles) {
        this.seedProfiles = seedProfiles;
    }

    public Long getVillageId() {
        return villageId;
    }

    public void setVillageId(Long villageId) {
        this.villageId = villageId;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) { this.cityId = cityId; }

    public Long getCompanyId() { return companyId; }

    public void setCompanyId(Long companyId) { this.companyId = companyId; }

    public Date getCreateTime() { return createTime;}

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}


