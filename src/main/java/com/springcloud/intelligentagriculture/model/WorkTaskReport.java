package com.springcloud.intelligentagriculture.model;



import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "work_task_report")
@EntityListeners(AuditingEntityListener.class)


public class WorkTaskReport implements Serializable {

    @ApiModelProperty(value = "工作任务汇报ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty(value = "工作任务ID")
    @NotNull
    private Long workTaskId;

    @ApiModelProperty(value = "发布日期")
    private Date reportTime;

    @ApiModelProperty(value = "汇报单位")
    private Long townId;

    @ApiModelProperty(value = "汇报人")
    @Length(max = 50, message = "*reportPerson must have at max 50 characters")
    private String reportPerson;

    @ApiModelProperty(value = "汇报内容")
    @Length(max = 1000, message = "*reportContent must have at max 1000 characters")
    private String reportContent;

    @ApiModelProperty(value = "工作任务附件")
    @Length(max = 1000, message = "*reportProfiles must have at max 1000 characters")
    private String reportProfiles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWorkTaskId() {
        return workTaskId;
    }

    public void setWorkTaskId(Long workTaskId) {
        this.workTaskId = workTaskId;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

    public String getReportPerson() {
        return reportPerson;
    }

    public void setReportPerson(String reportPerson) {
        this.reportPerson = reportPerson;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getReportProfiles() {
        return reportProfiles;
    }

    public void setReportProfiles(String reportProfiles) {
        this.reportProfiles = reportProfiles;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;
}