package com.springcloud.intelligentagriculture.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "product_property")
@EntityListeners(AuditingEntityListener.class)

public class ProductProperty implements Serializable {

    @ApiModelProperty(value = "属性ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "属性名称")
    @Length(max = 50, message = "*propertyName must have at max 50 characters")
    private String propertyName;

    @ApiModelProperty(value = "属性可选项 : {value1, value2, value3}")
    @Length(max = 255, message = "*propertyOptions must have at max 255 characters")
    private String propertyOptions;

    @ApiModelProperty(value = "属性排序")
    @Length(max = 255, message = "*propertySort must have at max 255 characters")
    private String propertySort;

    @ApiModelProperty(value = "产品ID : product_production")
    private Long productId;

    @ApiModelProperty(value = "是否共享(0:否，1：是)")
    @NotNull
    @Length(max = 255, message = "*doShare must have at max 255 characters")
    private String doShare;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新人")
    private Long updateUserId;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getPropertyName() { return propertyName; }

    public void setPropertyName(String propertyName) { this.propertyName = propertyName; }

    public String getPropertyOptions() { return propertyOptions; }

    public void setPropertyOptions(String propertyOptions) { this.propertyOptions = propertyOptions; }

    public String getPropertySort() { return propertySort; }

    public void setPropertySort(String propertySort) { this.propertySort = propertySort; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public String getDoShare() { return doShare; }

    public void setDoShare(String doShare) { this.doShare = doShare; }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

}
