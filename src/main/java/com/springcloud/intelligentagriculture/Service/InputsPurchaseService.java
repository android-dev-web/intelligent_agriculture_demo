package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.InputsPurchase;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.InputsPurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InputsPurchaseService
{
    @Autowired
    InputsPurchaseRepository repository;

    public List<InputsPurchase> getAllInputsPurchases(Integer pageNo, Integer pageSize, String sortBy)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<InputsPurchase> pagedResult = null;
        try{
            pagedResult = repository.findAll(paging);
        }
        catch (Exception e){
            return new ArrayList<InputsPurchase>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<InputsPurchase>();
        }
    }

}