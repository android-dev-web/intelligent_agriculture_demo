package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.ShareFilesRecord;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.ShareFilesRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShareFilesRecordService
{
    @Autowired
    ShareFilesRecordRepository repository;

    public List<ShareFilesRecord> getAllShareFilesRecords(Integer pageNo, Integer pageSize, String sortBy, Integer shareFilesId, String uploadCompany )
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<ShareFilesRecord> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, shareFilesId, uploadCompany);
        }
        catch (Exception e){
            return new ArrayList<ShareFilesRecord>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<ShareFilesRecord>();
        }
    }

}