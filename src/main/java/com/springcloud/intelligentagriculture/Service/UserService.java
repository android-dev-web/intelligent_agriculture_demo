package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.Users;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService
{
    @Autowired
    UserRepository repository;

    public List<Users> getAllUsers(Integer pageNo, Integer pageSize, String sortBy)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Users> pagedResult = null;
        try{
            pagedResult = repository.findAll(paging);
        }
        catch (Exception e){
            return new ArrayList<Users>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Users>();
        }
    }

}