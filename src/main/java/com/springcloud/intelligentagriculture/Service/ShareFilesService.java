package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.ShareFiles;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.ShareFilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShareFilesService
{
    @Autowired
    ShareFilesRepository repository;

    public List<ShareFiles> getAllShareFiless(Integer pageNo, Integer pageSize, String sortBy, int isVisible )
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<ShareFiles> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, isVisible);
        }
        catch (Exception e){
            return new ArrayList<ShareFiles>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<ShareFiles>();
        }
    }

}