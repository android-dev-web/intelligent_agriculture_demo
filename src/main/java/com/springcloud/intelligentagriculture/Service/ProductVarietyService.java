package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.*;
import com.springcloud.intelligentagriculture.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductVarietyService
{
    @Autowired
    ProductVarietyRepository repository;

    public List<ProductVariety> getAllProductVariety(Integer pageNo, Integer pageSize, String sortBy, Long productId)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<ProductVariety> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, productId);
        }
        catch (Exception e){
            return new ArrayList<ProductVariety>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<ProductVariety>();
        }
    }

}