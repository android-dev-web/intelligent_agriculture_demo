package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.Blacklist;
import com.springcloud.intelligentagriculture.model.TrainingFunds;
import com.springcloud.intelligentagriculture.repository.BlacklistRepository;
import com.springcloud.intelligentagriculture.repository.TrainingFundsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TrainingFundsService
{
    @Autowired
    TrainingFundsRepository repository;

    public List<TrainingFunds> getAllTrainingFunds(Integer pageNo, Integer pageSize, String sortBy, int specialFlag, String createTime, String createTimeFrom, String createTimeTo)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<TrainingFunds> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, specialFlag, createTime, createTimeFrom, createTimeTo);
//            pagedResult = repository.findAllBySearch(paging, specialFlag);
        }
        catch (Exception e){
            return new ArrayList<TrainingFunds>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<TrainingFunds>();
        }
    }

}