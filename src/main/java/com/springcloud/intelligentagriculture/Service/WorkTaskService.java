package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkTaskService
{
    @Autowired
    WorkTaskRepository repository;

    public List<WorkTask> getAllWorkTask(Integer pageNo, Integer pageSize, String sortBy, String releaseTimeFrom, String releaseTimeTo)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<WorkTask> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, releaseTimeFrom, releaseTimeTo);
        }
        catch (Exception e){
            return new ArrayList<WorkTask>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<WorkTask>();
        }
    }

}