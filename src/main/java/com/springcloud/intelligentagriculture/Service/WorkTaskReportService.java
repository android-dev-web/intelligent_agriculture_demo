package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.WorkTaskReportRepository;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkTaskReportService
{
    @Autowired
    WorkTaskReportRepository repository;

    public List<WorkTaskReport> getAllWorkTaskReport(Integer pageNo, Integer pageSize, String sortBy, Long workTaskId)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<WorkTaskReport> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, workTaskId);
        }
        catch (Exception e){
            return new ArrayList<WorkTaskReport>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<WorkTaskReport>();
        }
    }

}