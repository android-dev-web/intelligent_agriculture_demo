package com.springcloud.intelligentagriculture.Service;

import com.springcloud.intelligentagriculture.model.*;
import com.springcloud.intelligentagriculture.repository.InternalMessageRepository;
import com.springcloud.intelligentagriculture.repository.ProductionStandardRepository;
import com.springcloud.intelligentagriculture.repository.WarehouseRepository;
import com.springcloud.intelligentagriculture.model.Warehouse;
import com.springcloud.intelligentagriculture.repository.WorkTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InternalMessageService
{
    @Autowired
    InternalMessageRepository repository;

    public List<InternalMessage> getAllInternalMessage(Integer pageNo, Integer pageSize, String sortBy, Long receiveUserId, Long createUserId, Long receiveStatus)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<InternalMessage> pagedResult = null;
        try{
            pagedResult = repository.findAllBySearch(paging, receiveUserId, createUserId, receiveStatus);
        }
        catch (Exception e){
            return new ArrayList<InternalMessage>();
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<InternalMessage>();
        }
    }

}