package com.springcloud.intelligentagriculture.authenticate;

import com.springcloud.intelligentagriculture.model.Users;

import java.io.Serializable;
import java.util.Date;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private String contactName;
    private String contactPerson;
    private String contactWay;
    private Date createTime;


    public JwtResponse(String jwttoken, Users loginUser) {
        this.jwttoken = jwttoken;
        this.contactName = loginUser.getContactName();
        this.contactPerson = loginUser.getContactPerson();
        this.contactWay = loginUser.getContactWay();
        this.createTime = loginUser.getCreateTime();
    }

    public String getToken() {
        return this.jwttoken;
    }
}
